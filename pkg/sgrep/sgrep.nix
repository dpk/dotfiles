{ lib, stdenv }:

stdenv.mkDerivation rec {
  pname = "sgrep";
  version = "1.94a";

  src = fetchTarball {
    url = "https://deb.debian.org/debian/pool/main/s/sgrep/sgrep_1.94a.orig.tar.gz";
    sha256 = "1ivahngmkrq7l7cbl92h3xp5nz7c8saz9zd3bfinliblnmrfl75x";
  };
  patches = [ ./sgrep-1.94a-clang16.patch ];
}
