let
  pkgs = import <nixpkgs> {};
  sgrep = pkgs.callPackage ./sgrep.nix {};
in
pkgs.mkShell {
  buildInputs = [
    sgrep
  ];
}
