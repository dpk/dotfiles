let
  pkgs = import <nixpkgs> {};
  jdupes = pkgs.callPackage ./jdupes.nix {};
in
pkgs.mkShell {
  buildInputs = [
    jdupes
  ];
}
