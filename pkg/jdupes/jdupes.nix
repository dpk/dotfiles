{ pkgs }:

pkgs.jdupes.overrideAttrs(finalAttrs: previousAttrs: rec {
  makeFlags = previousAttrs.makeFlags ++ [
    "ENABLE_DEDUPE=1"
    "STATIC_DEDUPE_H=1"
  ];
})
