;; -*- lexical-binding: t; -*-
(add-to-list 'load-path "~/.emacs.d/lisp/")
(setq custom-file "~/.emacs.d/custom.el")
(load-file "~/.emacs.d/custom.el")

;;;; general emacs setup

;; customize the default frame
(add-to-list 'default-frame-alist '(width . 80))
(add-to-list 'default-frame-alist '(height . 1.0))
(add-to-list 'default-frame-alist '(font . "-*-Iosevka-regular-normal-expanded-*-11-*-*-*-m-0-iso10646-1"))

(add-to-list 'initial-frame-alist '(width . 80))
(add-to-list 'initial-frame-alist '(height . 1.0))
(add-to-list 'initial-frame-alist '(top . 0))
(add-to-list 'initial-frame-alist '(font . "-*-Iosevka-regular-normal-expanded-*-11-*-*-*-m-0-iso10646-1"))

;;; enable mouse terminal support
(require 'mouse)
(xterm-mouse-mode t)
(defun track-mouse (e))
(setq mouse-sel-mode t)
(setq mouse-wheel-mode t)

;;; scroll with mouse in terminal
;; (this causes garbage to be sprayed into the terminal window if you
;; scroll too high, for some reason)
(global-set-key [mouse-4] 'previous-line)
(global-set-key [mouse-5] 'next-line)

;;; don’t litter directories with backups and autosaves
;; keep them all in one place
(setq backup-directory-alist `(("." . "~/.saves")))
(setq auto-save-file-name-transforms `((".*" "~/.saves/" t)))

;;; set source-directory if it isn’t set up properly
(when (not (and (stringp source-directory)
                (file-exists-p
                 (concat (file-name-as-directory source-directory)
                         "src/emacs.c"))))
  (if-let ((maybe-source-directory (concat "~/.emacs.d/src/emacs-"
                                           emacs-version))
           (exists-p (file-directory-p maybe-source-directory)))
      (setq source-directory maybe-source-directory)
    (message "No Emacs source directory found")))

;;; package
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/"))
(add-to-list 'package-archives
             '("nongnu" . "https://elpa.nongnu.org/nongnu/"))
(package-initialize)

;; package go brrr
(setq package-native-compile t)
(setq native-comp-async-report-warnings-errors nil) ; shut up

;;; use fn as meta
(setq ns-function-modifier 'meta)

;; ... and allow us to bind alt for function keys, but don't affect its default meaning for letters etc.
(setq ns-alternate-modifier '(:ordinary none :function alt :mouse alt))

;;; workaround for Mac OS 12’s insistence on nabbing several useful
;;; fn+letter combinations for itself. bug FB9830836
(seq-doseq (letter "abcdefghijklmnopqrstuvwxyz")
  (if-let ((letter-str (make-string 1 letter))
           (key-seq (concat "C-S-" letter-str))
           (cmd (lookup-key global-map (kbd key-seq))))
      (message "Not clobbering existing binding of %s to %s for workaround"
               key-seq cmd)
    (define-key key-translation-map (kbd key-seq) (kbd (concat "M-" letter-str)))))
;; and for good measure, as it's easier to reach than the default (C-x @ m):
(define-key function-key-map (kbd "C-`") 'event-apply-meta-modifier)

;;; smex
(require 'smex)
(smex-initialize)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;;; ido-mode
(ido-mode t)
(setq ido-auto-merge-work-directories-length -1)

;;; which-key
(which-key-mode 1)
(setq which-key-idle-delay .0001)

;;; shut the bell up
(setq ring-bell-function 'ignore)

;;; disable copy-on-mouse-select
(setq mouse-drag-copy-region nil)

;;; echo uncomplete keystrokes basically instantaneously
(setq echo-keystrokes .0001)

;;; never use yes-or-no-p
(setq use-short-answers t)

;;; only one esc to quit sth
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "S-<escape>") 'keyboard-quit)

;;; defang repeat-complex-command
(global-set-key (kbd "C-x <escape>") 'ignore)

;;; always use spaces to indent
(setq-default indent-tabs-mode nil)

;;; disable the Emacs toolbar
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))

;;; delete existing selection when typing
(delete-selection-mode 1)

;;; highlight matching parentheses
(show-paren-mode 1)
(setq show-paren-context-when-offscreen t)
(setq minibuffer-message-timeout nil)

;;; show all buffers in the buffers menu
(setq buffers-menu-max-size nil)

;;; start the server
(server-start)

;;; add home directory and nix binaries to Emacs’s PATH
(add-to-list 'exec-path "~/bin")
(setenv "PATH" (format "%s:%s" (expand-file-name "~/bin") (getenv "PATH")))

(add-to-list 'exec-path "~/.nix-profile/bin")
(setenv "PATH" (format "%s:%s" (expand-file-name "~/.nix-profile/bin") (getenv "PATH")))

;;;; custom editing commands and global key bindings

(when (fboundp 'ns-get-selection)
  ;;; copy and paste using the Mac OS clipboard (without shelling out!)
  (defun mac-os-paste ()
    (ns-get-selection 'CLIPBOARD 'STRING))

  (defun mac-os-copy (text)
    (ns-own-selection-internal 'CLIPBOARD text))

  ;; see so:22849281
  ;; (setq interprogram-paste-function 'mac-os-paste)
  ;; (setq interprogram-cut-function 'mac-os-copy)
  (setq interprogram-cut-function nil)
  (setq interprogram-paste-function nil)

  (defun pasteboard-copy ()
    "Copy region to Mac OS system pasteboard."
    (interactive)
    (mac-os-copy
     (buffer-substring-no-properties (region-beginning) (region-end))))
  (defun pasteboard-paste ()
    "Paste from Mac OS system pasteboard to point."
    (interactive)
    (let ((pb-contents (mac-os-paste)))
      (kill-region (point) (if mark-active (mark) (point)))
      (insert pb-contents)))
  (defun pasteboard-cut ()
    "Cut region and put on Mac OS system pasteboard."
    (interactive)
    (pasteboard-copy)
    (delete-region (point) (if mark-active (mark) (point))))

  (global-set-key (kbd "s-c") 'pasteboard-copy)
  (global-set-key (kbd "s-v") 'pasteboard-paste)
  (global-set-key (kbd "s-x") 'pasteboard-cut))

;;; set up undo-tree-mode
(require 'undo-tree)
(global-undo-tree-mode)
(global-set-key (kbd "s-z") 'undo-tree-undo)
(global-set-key (kbd "s-Z") 'undo-tree-redo)
(setq undo-tree-auto-save-history t)
(setq undo-tree-history-directory-alist `(("." . "~/.saves")))

(defun inhibit-messages-around-advice (fun &rest args)
  "Advice placed :around a function FUN to prevent it from putting
messages in the minibuffer. They will still appear in the
`*Messages*' buffer."
  (let ((inhibit-message t))
    (apply fun args)))

(advice-add 'undo-tree-save-history :around #'inhibit-messages-around-advice)

;;; select line
(defun select-current-line ()
  "Select the current line, including the newline character at the
end. If region is active, select from the beginning of the line
at the start of the region to the end of the line at the end of
it."
  (interactive)
  (let ((line-move-visual nil))
    (if (use-region-p)
        (progn
          (let ((rbeg (region-beginning))
                (rend (region-end)))
            (goto-char rbeg)
            (set-mark (line-beginning-position))
            (goto-char rend)
            (next-line)
            (beginning-of-line)))
      (progn
        (next-line)
        (beginning-of-line)
        (let ((lbgp (line-beginning-position)))
          (previous-line)
          (set-mark lbgp))
        (exchange-point-and-mark))))
  (mouse-set-region-1))
(global-set-key (kbd "s-l") 'select-current-line)
(global-set-key (kbd "s-j") 'goto-line)

;;; Mac OS native keybindings
(global-set-key (kbd "s-<up>") 'beginning-of-buffer)
(global-set-key (kbd "s-<down>") 'end-of-buffer)
(global-set-key (kbd "s-<left>") 'move-beginning-of-line)
(global-set-key (kbd "s-<right>") 'move-end-of-line)
(global-set-key (kbd "s-<backspace>") (lambda ()
                                        (interactive)
                                        (kill-line 0)))

(global-set-key (kbd "A-<up>") 'scroll-down-command)
(global-set-key (kbd "A-<down>") 'scroll-up-command)
(global-set-key (kbd "A-<left>") 'backward-word)
(global-set-key (kbd "A-<right>") 'forward-word)
(global-set-key (kbd "A-<backspace>") 'backward-kill-word)

;;; other assignments to Mac modifier keys
(global-set-key (kbd "A-s-<left>") 'backward-sexp)
(global-set-key (kbd "A-s-<right>") 'forward-sexp)
(global-set-key (kbd "A-s-<up>") 'backward-up-list)
(global-set-key (kbd "A-s-<down>") 'down-list)

;; set Cmd-squarebrackets to move up and down pages
(global-set-key (kbd "s-[") 'backward-page)
(global-set-key (kbd "s-]") 'forward-page)
;; set Cmd-Shift-brackets to cycle buffers
(global-set-key (kbd "s-{") 'previous-buffer)
(global-set-key (kbd "s-}") 'next-buffer)

;; Cmd+Shift combinations
(global-set-key (kbd "s-G") 'isearch-repeat-backward)

;; Cmd+Opt combinations
(global-set-key (kbd "s-ƒ") 'replace-string)
(global-set-key (kbd "M-s-ƒ") 'replace-regexp)

;; Cmd+N to create a new buffer, Cmd+Shift+N for a new frame
(defun create-new-buffer ()
  "Create a new buffer named *new*[num]."
  (interactive)
  (switch-to-buffer (generate-new-buffer-name "*new*")))
(global-set-key (kbd "s-n") 'create-new-buffer)
(global-set-key (kbd "s-N") 'make-frame)

;; set Cmd-W to kill buffer and Cmd-Opt-W to close window
(defun kill-current-buffer ()
  "Kill the current buffer"
  (interactive)
  (kill-buffer (current-buffer)))
(global-set-key (kbd "s-w") 'kill-current-buffer)
(global-set-key (kbd "s-∑") 'delete-frame)
(global-unset-key (kbd "s-k"))

;;; defang text-resizing
(substitute-key-definition 'mouse-wheel-text-scale nil (current-global-map))

;;; sexp selection command
(defun looking-at-sexp-p ()
  "t if the cursor is exactly at the start of a sexp, otherwise
  nil."
  (let ((here (point)))
    (save-excursion
      (forward-char)
      (condition-case nil
          (progn (backward-sexp) (= here (point)))
        (scan-error t)))))

(defun select-sexp ()
  (interactive)
  (cond ((use-region-p)
         (backward-up-list))
        ((not (looking-at-sexp-p))
         (backward-sexp)))
  (set-mark (point))
  (forward-sexp)
  (mouse-set-region-1))
(global-set-key (kbd "s-b") 'select-sexp)

(global-set-key (kbd "C-c l") 'mark-paragraph)

;;; keyboard shortcuts for different wrapping modes
;; smex suggested giving them keyboard shortcuts. good idea!
(global-set-key (kbd "C-c f a") 'auto-fill-mode)
(global-set-key (kbd "C-c f v") 'visual-line-mode)
(global-set-key (kbd "C-c f t") 'toggle-truncate-lines)

;;; fn+tab doesn't trigger M-<tab> for some reason, so use A-<tab>
(global-set-key (kbd "A-<tab>") 'completion-at-point)

;;; right click to bring up a contextual menu
(defun context-menu-open-at-mouse-point (event)
  (interactive "e")
  (mouse-set-point event)
  (context-menu-open))
(global-set-key (kbd "<down-mouse-3>") 'context-menu-open-at-mouse-point)

;;; open URL at mouse point
(defun open-url-at-mouse-point (event)
  (interactive "e")
  (mouse-set-point event)
  (if-let ((url (thing-at-point 'url)))
      (browse-url url)
    (error "Not a URL")))
(global-set-key (kbd "s-<mouse-1>") 'open-url-at-mouse-point)

;;; visit in Terminal/Finder
(defun visit-in-terminal ()
  "Visit the folder containing the current buffer's file in a
Terminal window."
  (interactive)
  (if (eq major-mode 'dired-mode)
      (call-process "/usr/bin/open" nil 0 nil "-a" "iTerm" "--" (dired-current-directory))
    (if-let ((file-path (buffer-file-name)))
        (let ((dir-path (file-name-directory file-path)))
          (call-process "/usr/bin/open" nil 0 nil "-a" "iTerm" "--" dir-path))
      (message "Buffer not visiting a file: %s" (buffer-name)))))

(defun show-in-finder ()
  "Reveal the current buffer's file in the Finder."
  (interactive)
  (if (eq major-mode 'dired-mode)
      (call-process "/usr/bin/open" nil 0 nil "--" (dired-current-directory))
    (if-let ((file-path (buffer-file-name)))
        (call-process "/usr/bin/open" nil 0 nil "-R" "--" file-path)
      (message "Buffer not visiting a file: %s" (buffer-name)))))

;;; when filling, don't convert one space after full stop to two
(setq sentence-end-double-space nil)

;;; titlecase and other casing commands
(require 'titlecase)
(global-set-key (kbd "C-x M-u") 'upcase-dwim)
(global-set-key (kbd "C-x M-l") 'downcase-dwim)
(global-set-key (kbd "C-x M-k") 'capitalize-dwim) ; M-c is already
                                                  ; taken by Mac OS as
                                                  ; of version 12
(global-set-key (kbd "C-x M-t") 'titlecase-dwim)

;;;; configure theme
(require 'vimspectr)
(add-to-list 'custom-theme-load-path  "~/.emacs.d/vimspectr-themes")
(setq custom-theme-directory "~/.emacs.d/themes")

(load-theme 'vimspectrgrey-light t)
(load-theme 'vimspectr210-dark t)
(load-theme 'smart-mode-line-light-powerline t)
(load-theme 'smart-mode-line-powerline t)

;;; handle theme changes in Polymode
(require 'polymode)
(defun pm--post-theme-change ()
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (when polymode-mode
        (save-excursion
          (let ((modifiedp (buffer-modified-p)))
            (pm-map-over-spans
             (lambda (span)
               (remove-list-of-text-properties (nth 1 span) (nth 2 span)
                                               '(face))))
            (set-buffer-modified-p modifiedp)))))))

;;; night- and daytime theme switcher
(defun turn-on-daytime-theme ()
  (interactive)
  (disable-theme 'smart-mode-line-powerline)
  (enable-theme 'smart-mode-line-light-powerline)
  (disable-theme 'vimspectr210-dark)
  (enable-theme 'vimspectrgrey-light)
  (pm--post-theme-change))
(turn-on-daytime-theme)

(defun turn-on-nighttime-theme ()
  (interactive)
  (disable-theme 'smart-mode-line-light-powerline)
  (enable-theme 'smart-mode-line-powerline)
  (disable-theme 'vimspectrgrey-light)
  (enable-theme 'vimspectr210-dark)
  (pm--post-theme-change))

(defun toggle-day-night ()
  (interactive)
  (if (memq 'vimspectr210-dark custom-enabled-themes)
      (turn-on-daytime-theme)
    (turn-on-nighttime-theme)))
(global-set-key (kbd "C-*") 'toggle-day-night)

(defun auto-change-day-night-theme (appearance)
  (cl-case appearance
    ((dark) (turn-on-nighttime-theme))
    ((light) (turn-on-daytime-theme))))

(when (boundp 'ns-system-appearance-change-functions)
  (add-hook 'ns-system-appearance-change-functions #'auto-change-day-night-theme))

;;; display line numbers globally
(global-display-line-numbers-mode 1)
(setq display-line-numbers-grow-only t)

;;; smooth scrolling everywhere
(pixel-scroll-precision-mode 1)

;;; smart mode line
(setq sml/theme 'light-powerline)
(sml/setup)

(setq sml/mule-info
      `(""
        (current-input-method
         (:propertize ("" current-input-method-title)
                      local-map ,mode-line-input-method-map
                      mouse-face mode-line-highlight))
        (:eval
         (propertize
          (cond
           ((eq buffer-file-coding-system 'utf-8-unix) "")
           ((eq (coding-system-eol-type buffer-file-coding-system) 0) "%z")
           (t "%Z"))))))
(setq sml/position-percentage-format nil)

;;; delight: hide the lighters of my global minor modes
(require 'delight)
(delight 'auto-revert-mode nil 'autorevert)
(delight 'pcre-mode nil 'pcre2el)
(delight 'undo-tree-mode nil 'undo-tree)
(delight 'which-key-mode nil 'which-key)

;;; basic ligature support setup macro
;; source: https://github.com/charJe/.emacs.d/blob/51a4cff1979d6351360702301dbc3d70cfc63810/charles/charles.el#L148
(defmacro tie (&rest compositions)
  "Tie some each item in COMPOSITIONS together.
Each item in COMPOSITIONS must start with the same first character.
Each compositions must be supported by the font."
  (let ((char (string-to-char (substring (car compositions) 0 1))))
    `(set-char-table-range composition-function-table ,char
                           '([,(regexp-opt compositions) 0 font-shape-gstring]))))

;;; support emoji
(set-fontset-font
 t 'symbol (font-spec :family "Apple Color Emoji") nil 'prepend)

;;; and use Unifont as a fallback font
(set-fontset-font "fontset-default" nil
                  (font-spec :size 18 :name "Unifont" :antialias nil))
(set-fontset-font "fontset-default" nil
                  (font-spec :size 18 :name "Unifont Upper" :antialias nil))

;;; bar style cursor
(setq-default cursor-type 'bar)

;;;; configuration of major modes with their specific minor modes

;;; configure whitespace-mode to automatically start with makefile-mode
(add-hook 'makefile-mode-hook 'whitespace-mode)

;;; psgml
;; ordering restriction: this must come before nxml-mode is loaded, to
;; avoid clobbering sgml-mode-fix
(add-to-list 'load-path "~/.emacs.d/lisp/psgml")
(require 'psgml)
(setq sgml-auto-activate-dtd t)
(autoload 'psgml-html-mode "psgml-html"
  "Derived major mode for editing HTML with PSGML (see `sgml-mode')." t)

(require 'psgml-indent)
(defun psgml-html-setup-indentation ()
  (setq sgml-indent-step 2)
  (setq sgml-element-indent-alist
        '(("dd" . 2)
          ("html" . 0)
          ("pre" . nil)))
  (setq sgml-element-indent-stag '("dd")))
(add-hook 'psgml-html-mode-hook 'psgml-html-setup-indentation)

(defun sgml-reindent-close-angle (arg)
  (interactive "p")
  (if (looking-back "^[[:blank:]]*</[^>]+" (line-beginning-position))
      (indent-for-tab-command)))
(advice-add 'sgml-close-angle :before #'sgml-reindent-close-angle)

(require 'psgml-complete)
(require 'psgml-sexp)
(define-key sgml-mode-map (kbd "A-s-<up>") 'sgml-backward-up-element)
(define-key sgml-mode-map (kbd "A-s-<down>") 'sgml-down-element)

;;; set up Polymode for CSS and JS inside an HTML file
(define-hostmode poly-psgml-html-hostmode
  :mode 'psgml-html-mode
  :protect-font-lock nil
  :protect-syntax t)

;; ;; too unstable to use atm
;; (define-innermode poly-psgml-html-js2-innermode
;;   :mode 'js2-mode
;;   :head-matcher "<[Ss][Cc][Rr][Ii][Pp][Tt][^>]*>"
;;   :tail-matcher "</[Ss][Cc][Rr][Ii][Pp][Tt][[:space:]]*>"
;;   :head-mode 'host
;;   :tail-mode 'host)

;; ... so use the less advanced javascript-mode instead, which plays
;; nicer with polymode.
(define-innermode poly-psgml-html-javascript-innermode
  :mode 'javascript-mode
  :head-matcher "<[Ss][Cc][Rr][Ii][Pp][Tt][^>]*>"
  :tail-matcher "</[Ss][Cc][Rr][Ii][Pp][Tt][[:space:]]*>"
  :head-mode 'host
  :tail-mode 'host)

(define-innermode poly-psgml-html-css-innermode
  :mode 'css-mode
  :head-matcher "<[Ss][Tt][Yy][Ll][Ee][^>]*>"
  :tail-matcher "</[Ss][Tt][Yy][Ll][Ee]*>"
  :head-mode 'host
  :tail-mode 'host)

(define-polymode poly-psgml-html-mode
  :hostmode 'poly-psgml-html-hostmode
  :innermodes '(poly-psgml-html-javascript-innermode
                poly-psgml-html-css-innermode))

(defun psgml-html-setup-validation ()
  (setq sgml-validate-command "vnugnu %2$s"))
(flycheck-define-checker vnu
  "HTML checker with VNU"
  :command ("vnu" "-" "/dev/stdin")
  :standard-input t
  :error-patterns
  ((error line-start (or "" (file-name))
          (optional ":"
                    (or line
                        (seq line "." column)
                        (seq line "-" end-line)
                        (seq line "." column "-" end-line "." end-column)))
          ": error"
          (optional " fatal")
          ": " (message)
          line-end)
   (warning line-start (or "" (file-name))
            (optional ":"
                      (or line
                          (seq line "." column)
                          (seq line "-" end-line)
                          (seq line "." column "-" end-line "." end-column)))
            ": info warning: " (message)
            line-end)
   (info line-start (or "" (file-name))
         (optional ":"
                   (or line
                       (seq line "." column)
                       (seq line "-" end-line)
                       (seq line "." column "-" end-line "." end-column)))
         ": info: " (message)
         line-end))
  :modes (psgml-html-mode))

(add-hook 'psgml-html-mode-hook 'psgml-html-setup-validation)

;; set up psgml-html-mode to be used automatically for HTML files
(add-to-list 'auto-mode-alist
             '("\\.s?html?\\(\\.[a-zA-Z_]+\\)?\\'" . poly-psgml-html-mode))
(add-to-list 'magic-fallback-mode-alist
             '("<![Dd][Oo][Cc][Tt][Yy][Pp][Ee] [Hh][Tt][Mm][Ll]" . poly-psgml-html-mode))
(add-to-list 'magic-fallback-mode-alist
             '("<[Hh][Tt][Mm][Ll]" . poly-psgml-html-mode))
(setq psgml-base-directory "~/.emacs.d/dtds")

;;; nxml-mode customizations
(require 'sgml-mode-fix)
(with-eval-after-load 'nxml-mode
  (setq nxml-slash-auto-complete-flag t)
  (add-to-list 'rng-schema-locating-files "schemas.xml")
  (add-to-list 'rng-schema-locating-files "~/.emacs.d/schemas.xml"))
(add-to-list 'auto-mode-alist
             '("\\.\\(x[sm]l\\|svgz?\\|glif\\|plist\\)\\'" . nxml-mode))
(add-to-list 'magic-fallback-mode-alist
             '("<\\?xml" . nxml-mode))

;;; disable all other HTML, SGML, and XML editing modes
(setq auto-mode-alist
      (seq-remove
       (lambda (x) (memq (cdr x) '(html-helper-mode
                                   html-mode
                                   xml-mode
                                   sgml-mode
                                   mhtml-mode)))
       auto-mode-alist))
(setq magic-fallback-mode-alist
      (seq-remove
       (lambda (x) (memq (cdr x) '(html-helper-mode
                                   html-mode
                                   xml-mode
                                   sgml-mode
                                   mhtml-mode)))
       magic-fallback-mode-alist))

;;; rnc-mode
(add-to-list 'auto-mode-alist
             '("\\.rnc" . rnc-mode))

;;; use js2-mode for JavaScript
(add-to-list 'auto-mode-alist '("\\.[em]?js\\'" . js2-mode))

;;; Scheme mode
(require 'scheme)
;; someone prior to 1990 decided this (from scheme.el) was ‘too slow’
;; to be the default implementation of scheme-let-indent. over thirty
;; years of Moore's Law later, i think my CPU can handle it.
(defun scheme-would-be-symbol (string)
  (not (string-equal (substring string 0 1) "(")))

(defun scheme-next-sexp-as-string ()
  ;; Assumes that it is protected by a save-excursion
  (forward-sexp 1)
  (let ((the-end (point)))
    (backward-sexp 1)
    (buffer-substring (point) the-end)))

(defun scheme-let-indent (state indent-point normal-indent)
 (if (scheme-would-be-symbol (scheme-next-sexp-as-string))
     (lisp-indent-specform 2 state indent-point normal-indent)
   (lisp-indent-specform 1 state indent-point normal-indent)))

;; some extra Scheme macros
(put 'with-ellipsis 'scheme-indent-function 1)
(put 'match 'scheme-indent-function 1)
;; R7RS forms of Scheme macros
(put 'syntax-rules 'scheme-indent-function 'scheme-let-indent)

(defun lisp-no-indent-form-feed (fun &rest args)
  "Advice for `lisp-indent-line' to always put a form feed at column
0. (Mostly useful in R6RS Scheme library forms.)"
  (let ((avoided-indentp))
    (save-excursion
      (beginning-of-line)
      (cond ((looking-at-p "^$")
             (indent-line-to 0)
             (setq avoided-indentp t))
            ((looking-at-p "^")
             (message "Warning: nonempty line %d starts with form feed" (line-number-at-pos)))))
    (unless avoided-indentp (apply fun args))))
(advice-add 'lisp-indent-line :around #'lisp-no-indent-form-feed)

(require 'scheme-complete)
(define-key scheme-mode-map (kbd "A-<tab>") #'scheme-smart-complete)
(defun scheme-complete-setup-eldoc ()
  (make-local-variable 'eldoc-documentation-function)
  (setq eldoc-documentation-function 'scheme-get-current-symbol-info)
  (eldoc-mode))
(add-hook 'scheme-mode-hook #'scheme-complete-setup-eldoc)

(add-hook 'scheme-mode-hook #'outline-minor-mode)

;; ligatures for Scheme
(defun scheme-setup-ligatures ()
  (setq-local composition-function-table (copy-sequence composition-function-table))
  (tie "=>")
  (tie ">=")
  (tie "->")
  (tie "<="))
(add-hook 'scheme-mode-hook #'scheme-setup-ligatures)

(defun scheme-insert-lambda (n)
  (interactive "p")
  (self-insert-command n ?λ))
(define-key scheme-mode-map (kbd "¬") #'scheme-insert-lambda)

;; R7RS library definition files
(add-to-list 'auto-mode-alist '("\\.sld\\'" . scheme-mode))
;; R6RS program files
(add-to-list 'auto-mode-alist '("\\.sps\\'" . scheme-mode))

;;; set up Geiser
(require 'geiser)
(require 'geiser-mode)

(setq geiser-mode-auto-p nil)

;; make Geiser behave properly in Polymode buffers
(defun geiser-polymode-eval-buffer (&optional and-go raw nomsg)
  "Eval only the Scheme-containing parts of the current buffer in
the Geiser REPL. See `geiser-eval-buffer'."
  (interactive "P")
  (if (and (boundp 'polymode-mode) polymode-mode)
      (let* ((bounds (geiser-eval--bounds geiser-impl--implementation))
             (from (or (car bounds) (point-min)))
             (to (or (cdr bounds) (point-max))))
        (save-excursion
          (pm-map-over-spans (lambda (span)
                               (if (and (eq (car span) 'body)
                                        (eq (pm-span-mode span) 'scheme-mode))
                                   (save-excursion
                                     (geiser-eval-region (nth 1 span)
                                                         (nth 2 span)
                                                         and-go raw nomsg))))
                             from to)))
    (geiser-eval-buffer and-go raw nomsg)))
(define-key geiser-mode-map (kbd "C-c C-b") 'geiser-polymode-eval-buffer)

(setenv "CHEZSCHEMELIBDIRS" (expand-file-name "~/.chez-lib:"))

;;; Racket Mode
(add-to-list 'load-path "~/.emacs.d/racket-mode")
(require 'racket-mode)

(add-to-list 'auto-mode-alist '("\\.\\(rkt\\|scrbl\\)\\'" . racket-mode))
;(add-hook 'racket-mode-hook #'racket-hash-lang-mode)

;;; other Lisp modes
(add-hook 'lisp-mode-hook #'outline-minor-mode)
(add-hook 'emacs-lisp-mode-hook #'outline-minor-mode)

;;; Haskell mode
(defun haskell-setup-ligatures ()
  (setq-local composition-function-table (copy-sequence composition-function-table))
  (tie "<--" "<---" "<<-" "<-" "<==" "<===" "<<=" "<=" "<->" "<-->" "<--->" "<---->" "<=>" "<==>" "<===>" "<====>" "<~~" "<>" "<:" "<*" "<*>" "<|" "<|>" "<." "<.>")
  (tie "-<<" "-<" "-<-" "->" "->>" "-->" "--->" "->-" "-|")
  (tie ">-" ">>-" ">=" ">>=")
  (tie "=<<" "=<" "=<=" "=>" "=>>" "==>" "===>" "=>=" "===" "=/=" "=:" "=*")
  (tie "::" ":::" ":=" ":>")
  (tie "~~>")
  (tie "/=" "/\\")
  (tie "++" "+++" "+*")
  (tie "|-" "|>")
  (tie "*=" "*+" "*>"))
(add-hook 'haskell-mode-hook #'haskell-setup-ligatures)

;;; Markdown mode
(with-eval-after-load 'markdown-mode
  (define-key markdown-mode-map (kbd "<M-right>") nil)
  (define-key markdown-mode-map (kbd "<M-left>") nil)
  (delight 'markdown-mode "M↓" :major))

;; use poly-markdown-mode, not vanilla markdown-mode
(add-to-list 'auto-mode-alist '("\\.\\(?:md\\|markdown\\|mkd\\|mdown\\|mkdn\\|mdwn\\|text\\)\\'" . poly-markdown-mode))

;;; Org mode
(with-eval-after-load 'org
    (setq org-support-shift-select 'always) ; select text with shift +
                                            ; arrow keys in org-mode
    (setq org-adapt-indentation nil)) ; don’t indent text under
                                        ; headings
;; put TeX Live in PATH so Org mode export can find it
(setenv "PATH" (format "%s:%s" "/Library/TeX/texbin" (getenv "PATH")))

;;; Dired: use ls emulation
(setq dired-use-ls-dired nil)
(setq ls-lisp-use-insert-directory-program nil)
(require 'ls-lisp)

;;; lilypond-mode
(autoload 'LilyPond-mode "lilypond-mode.el" "LilyPond editing mode" t)
(add-to-list 'auto-mode-alist '("\\.i?ly\\'" . LilyPond-mode))

;;; gregorio-mode
(add-to-list 'auto-mode-alist '("\\.gabc\\'" . gregorio-mode))

;;; comint
(setq comint-prompt-read-only t)
(add-hook 'shell-mode-hook #'(lambda ()
                               (setq comint-process-echoes t)))
(define-key comint-mode-map (kbd "s-k") 'comint-clear-buffer)

;;; shell
(setq shell-file-name (expand-file-name "~/.nix-profile/bin/es"))

;;; Load OED2
(autoload 'oed2 "oed2.el" "OED2" t)
(with-eval-after-load 'oed2
  (setq oed2-sgml-location "~/share/OED2.sgml")
  (setq oed2-index-location "~/share/OED2.idx")
  (setq oed2-headword-index-location "~/share/oed2-headword-index.el.gz"))

;;;; general minor mode configuration

;;; don't highlight long lines in whitespace-mode
(with-eval-after-load 'whitespace
  (setq whitespace-style (remove 'lines whitespace-style)))

;;; outline-mode
(with-eval-after-load 'outline
  (define-key outline-minor-mode-map (kbd "C-<tab>") 'outline-cycle)
  (define-key outline-minor-mode-map (kbd "C-S-<tab>") 'outline-cycle-buffer))

;;; my custom hacked wc-mode
(autoload 'wc-mode "wc-mode.el" "wc-mode" t)

;;; add smart-quotes mode
(autoload 'smart-quotes-mode "smart-quotes.el" "Smart quotes" t)
(with-eval-after-load 'smart-quotes
  (setq smart-quotes-reverse-quotes nil)
  ;; fixes for my idiosyncratic typing style, and also for all unicode
  ;; non-word chars
  (setq smart-quotes-left-context "^\\|[^[:word:]“‘.,;]"))

;;; Wolfram Alpha (autoloads)
(global-set-key (kbd "s-?") 'wolfram-alpha) ;; C-h i for info

;;; Magit
(require 'magit)
(defun magit-open ()
  (interactive)
  (magit-call-git "open"))
(define-key magit-mode-map (kbd "`") 'magit-open)

(defun magit-auto-boring-commit-message ()
  (let ((staged-files (magit-staged-files)))
    (if (= (length staged-files) 1)
        (format "Updated %s\n" (car staged-files))
      (format "Updated %d files\n\n%s"
              (length staged-files)
              (string-join staged-files "\n")))))

(defvar-local magit-auto-llm-commit-message-context 3)
(defvar-local magit-auto-llm-commit-message-diff nil)
(defun magit-auto-llm-commit-message (&optional diff-src)
  (with-demoted-errors
      "LLM error: %S"
    (let ((diff (or diff-src magit-auto-llm-commit-message-diff)))
      (with-temp-buffer
        (call-process "~/.nix-profile/bin/ollama" nil '(t nil) nil
                      "run" "mannix/gemma2-9b-simpo"
                      (format
                       "Write a very short commit message for this diff.\n\n```\n%s\n```"
                       diff))
        (beginning-of-buffer)
        (let ((fill-column 1000))
          (fill-paragraph))
        (string-trim
         (buffer-substring-no-properties (point-min) (point-max)))))))

(defun magit-diff-for-staged ()
  (let ((ctx magit-auto-llm-commit-message-context))
    (with-temp-buffer
      (magit-process-git t
                         "diff"
                         "--staged"
                         (format "-U%d" ctx))
      (buffer-substring-no-properties (point-min) (point-max)))))

(defun magit-commit-all-unstaged-changes ()
  (interactive)
  (magit-stage-modified)
  (if (null (magit-staged-files))
      (message "No changes to commit (check for new files)")
    (let* ((boring-message (magit-auto-boring-commit-message))
           (diff-src (magit-diff-for-staged))
           (llm-generated-message (magit-auto-llm-commit-message diff-src)))
      (when llm-generated-message
        (ring-insert log-edit-comment-ring boring-message))
      (magit-commit-create `("--edit"
                             "--message"
                             ,(or llm-generated-message boring-message))))))
(define-key magit-mode-map (kbd "C-s") 'magit-commit-all-unstaged-changes)

(defun git-commit-llm-commit-message-setup ()
  (when (null magit-auto-llm-commit-message-diff)
    (setq magit-auto-llm-commit-message-diff (magit-diff-for-staged))))

(defun git-commit-llm-commit-message-widen-context ()
  (interactive)
  (setq magit-auto-llm-commit-message-context
        (round (* magit-auto-llm-commit-message-context 1.5)))
  (message "Trying %d lines" magit-auto-llm-commit-message-context)
  (setq magit-auto-llm-commit-message-diff (magit-diff-for-staged))
  (git-commit-llm-commit-message))
(defun git-commit-llm-commit-message-narrow-context ()
  (interactive)
  (setq magit-auto-llm-commit-message-context
        (round (* magit-auto-llm-commit-message-context 0.66667)))
  (message "Trying %d lines" magit-auto-llm-commit-message-context)
  (setq magit-auto-llm-commit-message-diff (magit-diff-for-staged))
  (git-commit-llm-commit-message))

(defun git-commit-llm-commit-message ()
  (interactive)
  (git-commit-llm-commit-message-setup)
  (let ((llm-generated-message (magit-auto-llm-commit-message)))
    (git-commit-save-message)
    (save-restriction
      (goto-char (point-min))
      (narrow-to-region
       (point)
       (if (re-search-forward (concat "^" comment-start) nil t)
           (max 1 (- (point) 2))
         (point-max)))
      (delete-region (point-min) (point)))
    (insert llm-generated-message)
    (insert "\n")
    (goto-char (point-min))))
(define-key git-commit-mode-map (kbd "C-c a i") #'git-commit-llm-commit-message)
(define-key git-commit-mode-map (kbd "C-c a +") #'git-commit-llm-commit-message-widen-context)
(define-key git-commit-mode-map (kbd "C-c a -") #'git-commit-llm-commit-message-narrow-context)

;;; don't save quite so much undo information for files in Git
(defun magit-undo-tree-reduce-limit ()
  (when (and (file-exists-p buffer-file-name)
             (magit-file-tracked-p buffer-file-name))
    (setq-local undo-limit 16000000)
    (setq-local undo-strong-limit 24000000)))
(add-hook 'find-file-hook #'magit-undo-tree-reduce-limit)

;;; typescript-ts-mode + tide-mode
(add-to-list 'treesit-language-source-alist
	     '(typescript . ("https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")))
(add-to-list 'treesit-language-source-alist
	     '(tsx . ("https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")))
(add-to-list 'auto-mode-alist
             '("\\.ts\\'" . typescript-ts-mode))

(defun tide-completion-at-point ()
  (let ((prefix (progn (looking-back "[a-zA-Z_$]\*" 50 t) (match-string 0))))
    (tide-command:completions prefix
      (lambda (response)
        (completion-in-region
         (- (point) (length prefix)) (point)
         (cl-loop for completion in response
                  if (string-prefix-p prefix completion)
                  collect completion))))))

(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  ;(setq-local flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (make-local-variable 'completion-at-point-functions)
  (push (lambda () 'tide-completion-at-point) completion-at-point-functions))
(add-hook 'typescript-ts-mode-hook #'setup-tide-mode)
(add-hook 'tsx-ts-mode-hook #'setup-tide-mode)

;; LSP mode
(setq lsp-keymap-prefix "s-\\")

;;; mu4e
(add-to-list 'load-path "~/.nix-profile/share/emacs/site-lisp/elpa/mu4e-1.12.6")
(require 'mu4e)
(setq mu4e-get-mail-command "~/.nix-profile/bin/offlineimap")
(setq
 mu4e-sent-folder   "/Sent Items"
 mu4e-drafts-folder "/Drafts"
 mu4e-trash-folder  "/Trash"
 mu4e-refile-folder "/Archive")


;;;; input methods (quail)
(require 'quail-indo-european)

;;;; load customizations containing private data
(if (file-exists-p "~/.emacs.d/init.private.el")
    (load-file "~/.emacs.d/init.private.el"))
