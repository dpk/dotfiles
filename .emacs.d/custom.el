(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aquamacs-customization-version-id 0 t)
 '(column-number-mode t)
 '(custom-safe-themes
   '("e37104ea331fba72468f50eaf53f179401e4424aa67bfd189d8e430ad26d8c57" "98ba640da3562464b7fcb7914c77cdd8fa8223bcb792560eea1df6c30cf51ee9" "b0cfe5fcb9cbe7e5519cce3aaacbd8b33bc0981d33a85349ff877dc9ec9b1787" "af3492cdf1fe1f1bed12fc223e99ac247f1360f6166f5392eaed04ee5f7f6519" "d96193ad44b0659d2373bacdc81f9507d2cc00c29d5c96e0d9c03baf37641db8" "2540a2fbf23f9dc5f25cf1f6b06361848e4acb27d80906c7d30cb4b4a15ef6e4" "67f0917214a43f406eb469f043c7d831e79b5ec938e75a1fc31ee898114d2c93" "ed54dda3a2cfcae6680f44e2f85e7c16a41459175f6a047392075c130ee30126" "8b52981fe1a471d6c82659e2104477c55930719d4094824329ca4691100669d9" "b9e9ba5aeedcc5ba8be99f1cc9301f6679912910ff92fdf7980929c2fc83ab4d" default))
 '(inhibit-startup-screen t)
 '(initial-buffer-choice t)
 '(package-selected-packages
   '(lsp-mode tide dockerfile-mode tuareg graphviz-dot-mode magit haskell-mode nix-mode which-key nxml-uxml poly-R ess ivy ac-js2 powerline smart-mode-line-powerline-theme delight package-lint smart-mode-line geiser-racket geiser-guile geiser-chez geiser poly-markdown polymode undo-tree wolfram htmlize org-plus-contrib scss-mode markdown-mode+ markdown-mode gregorio-mode sudo-edit pcre2el web-mode yatex jinja2-mode js2-mode yaml-mode smex rust-mode rnc-mode rainbow-mode mediawiki less-css-mode git-gutter gist))
 '(pcre-mode t)
 '(safe-local-variable-values
   '((eval add-to-list 'sgml-element-indent-alist
           '("body" . 0))
     (eval lsp)
     (eval put 'element 'scheme-indent-function nil)))
 '(sml/name-width '(0 . 40))
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:height 110 :width expanded :family "Iosevka"))))
 '(erc-timestamp-face ((t (:foreground "#999999" :weight bold))))
 '(fixed-pitch ((t (:family "Iosevka"))))
 '(latex-mode-default ((t (:inherit tex-mode-default))))
 '(org-footnote ((t (:inherit link))))
 '(racket-keyword-argument-face ((t (:inherit font-lock-keyword-face))))
 '(text-mode-default ((t (:inherit default))))
 '(tide-hl-identifier-face ((t (:inherit nil :underline "#777")))))
