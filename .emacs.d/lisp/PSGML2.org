#+TITLE: PSGML 2.0

Not currently a real project, but here’s what I’d consider to-do if I
were to pick it up.

* Put up-to-date versions on ELPA/Non-GNU ELPA/MELPA

Self-explanatory. It’s annoying having to grab updates from the Debian
repositories because they’re the only ones still making this work on
modern Emacs.

* Fix the name clash with ~sgml-mode~

Change the prefix of PSGML functions and variables to ~psgml~ instead
of ~sgml~, so it doesn’t clobber Emacs’s built-in ~sgml-mode~ (and by
extension a bunch of things that depend on it, like ~css-mode~).

* Use ~completion-at-point~ instead of ~sgml-complete~

As in [[file:psgml-complete.el][~psgml-complete~]].

* Use [[file:psgml-indent.el][~psgml-indent~]] by default

* Native support for the HTML5 doctype

* Bind ~forward-sexp-function~ instead of replacing the keybindings of ~forward-sexp~ and ~backward-sexp~

As in [[file:psgml-sexp.el][~psgml-sexp.el~]]. Unfortunately ~up-list~, ~backward-up-list~,
and ~down-list~ can’t similarly be easily customized, so we’re stuck
with replacing their keybindings.

* Better ~font-lock~ integration/better highlighting customization

Not sure how to do this. One of the last releases of PSGML added the
ability to customize the highlighting of elements using a processing
instruction in the DTD.

* An alternative approach

Develop a mode specialized to HTML (and only HTML) that doesn’t suck.
