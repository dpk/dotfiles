;; this file defines the functions and variables from the built-in
;; Emacs sgml-mode which are required by other modes. all others are
;; private-namespaced with the -- convention


;; sgml-tag-syntax-table is needed by nxml-mode.el and nxml-rap.el
(defvar sgml--specials '(?\" ?\')
  "List of characters that have a special meaning for SGML mode.
This list is used when first loading the `sgml-mode' library.
The supported characters are ?\\\", ?\\=', and ?-.

Including ?- makes double dashes into comment delimiters, but
they are really only supposed to delimit comments within DTD
definitions.  So we normally turn it off.")

(defun sgml--make-syntax-table (specials)
  (let ((table (make-syntax-table text-mode-syntax-table)))
    (modify-syntax-entry ?< "(>" table)
    (modify-syntax-entry ?> ")<" table)
    (modify-syntax-entry ?: "_" table)
    (modify-syntax-entry ?_ "_" table)
    (modify-syntax-entry ?. "_" table)
    (if (memq ?- specials)
	(modify-syntax-entry ?- "_ 1234" table))
    (if (memq ?\" specials)
	(modify-syntax-entry ?\" "\"\"" table))
    (if (memq ?' specials)
	(modify-syntax-entry ?\' "\"'" table))
    table))

(defconst sgml-tag-syntax-table
  (let ((table (sgml--make-syntax-table sgml--specials)))
    (dolist (char '(?\( ?\) ?\{ ?\} ?\[ ?\] ?$ ?% ?& ?* ?+ ?/))
      (modify-syntax-entry char "." table))
    (unless (memq ?' sgml--specials)
      ;; Avoid that skipping a tag backwards skips any "'" prefixing it.
      (modify-syntax-entry ?' "w" table))
    table)
  "Syntax table used to parse SGML tags.")

;; sgml-electric-tag-pair-mode is needed by rng-nxml.el
(defvar sgml-electric-tag-pair-overlays nil)
(defvar sgml-electric-tag-pair-timer nil)

(defun sgml-electric-tag-pair-before-change-function (_beg end)
  (condition-case err
  (save-excursion
    (goto-char end)
    (skip-chars-backward "-[:alnum:]_.:")
    (if (and ;; (<= (point) beg) ; This poses problems for downcase-word.
             (or (eq (char-before) ?<)
                 (and (eq (char-before) ?/)
                      (eq (char-before (1- (point))) ?<)))
             (null (get-char-property (point) 'text-clones)))
        (let* ((endp (eq (char-before) ?/))
               (cl-start (point))
	       (cl-end (progn (skip-chars-forward "-[:alnum:]_.:") (point)))
               (match
                (if endp
                    (when (sgml--skip-tag-backward 1) (forward-char 1) t)
                  (with-syntax-table sgml-tag-syntax-table
                    (let ((forward-sexp-function nil))
                      (up-list -1)
                      (when (sgml--skip-tag-forward 1)
                        (backward-sexp 1)
                        (forward-char 2)
                        t)))))
               (clones (get-char-property (point) 'text-clones)))
          (when (and match
                     (/= cl-end cl-start)
                     (equal (buffer-substring cl-start cl-end)
                            (buffer-substring (point)
                                              (save-excursion
						(skip-chars-forward
						 "-[:alnum:]_.:")
                                                (point))))
                     (or (not endp) (eq (char-after cl-end) ?>)))
            (when clones
              (message "sgml-electric-tag-pair-before-change-function: deleting old OLs")
              (mapc 'delete-overlay clones))
            (message "sgml-electric-tag-pair-before-change-function: new clone")
            (text-clone-create cl-start cl-end 'spread "[[:alnum:]-_.:]+")
            (setq sgml-electric-tag-pair-overlays
                  (append (get-char-property (point) 'text-clones)
                          sgml-electric-tag-pair-overlays))))))
  (scan-error nil)
  (error (message "Error in sgml-electric-pair-mode: %s" err))))

(defun sgml-electric-tag-pair-flush-overlays ()
  (while sgml-electric-tag-pair-overlays
    (delete-overlay (pop sgml-electric-tag-pair-overlays))))

(define-minor-mode sgml-electric-tag-pair-mode
  "Toggle SGML Electric Tag Pair mode.

SGML Electric Tag Pair mode is a buffer-local minor mode for use
with `sgml-mode' and related major modes.  When enabled, editing
an opening markup tag automatically updates the closing tag."
  :lighter "/e"
  (if sgml-electric-tag-pair-mode
      (progn
        (add-hook 'before-change-functions
                  'sgml-electric-tag-pair-before-change-function
                  nil t)
        (unless sgml-electric-tag-pair-timer
          (setq sgml-electric-tag-pair-timer
                (run-with-idle-timer 5 'repeat 'sgml-electric-tag-pair-flush-overlays))))
    (remove-hook 'before-change-functions
                 'sgml-electric-tag-pair-before-change-function
                 t)
    ;; We leave the timer running for other buffers.
    ))

(defun sgml--skip-tag-forward (arg)
  "Skip to end of tag or matching closing tag if present.
With prefix argument ARG, repeat this ARG times.
Return t if after a closing tag."
  (interactive "p")
  ;; FIXME: Use sgml-get-context or something similar.
  ;; It currently might jump to an unrelated </P> if the <P>
  ;; we're skipping has no matching </P>.
  (let ((return t))
    (with-syntax-table sgml-tag-syntax-table
      (while (>= arg 1)
	(skip-chars-forward "^<>")
	(if (eq (following-char) ?>)
	    (up-list -1))
	(if (looking-at "<\\([^/ \n\t>]+\\)\\([^>]*[^/>]\\)?>")
	    ;; start tag, skip any nested same pairs _and_ closing tag
	    (let ((case-fold-search t)
		  (re (concat "</?" (regexp-quote (match-string 1))
			      ;; Ignore empty tags like <foo/>.
			      "\\([^>]*[^/>]\\)?>"))
		  point close)
	      (forward-list 1)
	      (setq point (point))
	      ;; FIXME: This re-search-forward will mistakenly match
	      ;; tag-like text inside attributes.
	      (while (and (re-search-forward re nil t)
			  (not (setq close
				     (eq (char-after (1+ (match-beginning 0))) ?/)))
			  (goto-char (match-beginning 0))
			  (sgml--skip-tag-forward 1))
		(setq close nil))
	      (unless close
		(goto-char point)
		(setq return nil)))
	  (forward-list 1))
	(setq arg (1- arg)))
      return)))
(defun sgml--skip-tag-backward (arg)
  "Skip to beginning of tag or matching opening tag if present.
With prefix argument ARG, repeat this ARG times.
Return non-nil if we skipped over matched tags."
  (interactive "p")
  ;; FIXME: use sgml-get-context or something similar.
  (let ((return t))
    (while (>= arg 1)
      (search-backward "<" nil t)
      (if (looking-at "</\\([^ \n\t>]+\\)")
          ;; end tag, skip any nested pairs
          (let ((case-fold-search t)
                (re (concat "</?" (regexp-quote (match-string 1))
                            ;; Ignore empty tags like <foo/>.
                            "\\([^>]*[^/>]\\)?>")))
            (while (and (re-search-backward re nil t)
                        (eq (char-after (1+ (point))) ?/))
              (forward-char 1)
              (sgml--skip-tag-backward 1)))
        (setq return nil))
      (setq arg (1- arg)))
    return))

;; sgml-syntax-propertize is needed by nxml-mode.el
(defun sgml--syntax-propertize-ppss (pos)
  "Return PPSS at POS, fixing the syntax of any lone `>' along the way."
  (cl-assert (>= pos (car sgml--syntax-propertize-ppss)))
  (let ((ppss (parse-partial-sexp (car sgml--syntax-propertize-ppss) pos -1
                                  nil (cdr sgml--syntax-propertize-ppss))))
    (while (eq -1 (car ppss))
      (put-text-property (1- (point)) (point)
                         'syntax-table (string-to-syntax "."))
      ;; Hack attack: rather than recompute the ppss from
      ;; (car sgml--syntax-propertize-ppss), we manually "fix it".
      (setcar ppss 0)
      (setq ppss (parse-partial-sexp (point) pos -1 nil ppss)))
    (setcdr sgml--syntax-propertize-ppss ppss)
    (setcar sgml--syntax-propertize-ppss pos)
    ppss))

(eval-and-compile
  (defconst sgml-syntax-propertize-rules
    (syntax-propertize-precompile-rules
     ;; Use the `b' style of comments to avoid interference with the -- ... --
     ;; comments recognized when `sgml-specials' includes ?-.
     ;; FIXME: beware of <!--> blabla <!--> !!
     ("\\(<\\)!--" (1 "< b"))
     ("--[ \t\n]*\\(>\\)" (1 "> b"))
     ("\\(<\\)[?!]" (1 (prog1 "|>"
                         (sgml-syntax-propertize-inside end))))
     ;; Quotes outside of tags should not introduce strings which end up
     ;; hiding tags.  We used to test every quote and mark it as "."
     ;; if it's outside of tags, but there are too many quotes and
     ;; the resulting number of calls to syntax-ppss made it too slow
     ;; (bug#33887), so we're now careful to leave alone any pair
     ;; of quotes that doesn't hold a < or > char, which is the vast majority:
     ;; either they're both within a tag (or a comment), in which case it's
     ;; indeed correct to leave them as is, or they're both outside of tags, in
     ;; which case they arguably should have punctuation syntax, but it is
     ;; harmless to let them have string syntax because they won't "hide" any
     ;; tag or comment from us (and we use the
     ;; font-lock-syntactic-face-function to make sure those spurious "strings
     ;; within text" aren't highlighted as strings).
     ("\\([\"']\\)[^\"'<>]*"
      (1 (if (eq (char-after) (char-after (match-beginning 0)))
             ;; Fast-track case.
             (forward-char 1)
           ;; Point has moved to the end of the text we matched after the
           ;; quote, but this risks overlooking a match to one of the other
           ;; regexp in the rules.  We could just (goto-char (match-end 1))
           ;; to solve this, but that would be too easy, so instead we
           ;; only move back enough to avoid skipping comment ender, which
           ;; happens to be the only one that we could have overlooked.
           (when (eq (char-after) ?>)
             (skip-chars-backward "-"))
           ;; Be careful to call `syntax-ppss' on a position before the one
           ;; we're going to change, so as not to need to flush the data we
           ;; just computed.
           (if (zerop (save-excursion
                        (car (sgml--syntax-propertize-ppss
                              (match-beginning 0)))))
               (string-to-syntax ".")))))
     )
    "Syntax-propertize rules for sgml text.
These have to be run via `sgml-syntax-propertize'"))

(defconst sgml--syntax-propertize
  (syntax-propertize-rules sgml-syntax-propertize-rules))

(defun sgml-syntax-propertize (start end &optional rules-function)
  "Syntactic keywords for `sgml-mode'."
  (setq sgml--syntax-propertize-ppss (cons start (syntax-ppss start)))
  (cl-assert (>= (cadr sgml--syntax-propertize-ppss) 0))
  (sgml-syntax-propertize-inside end)
  (funcall (or rules-function sgml--syntax-propertize) (point) end)
  ;; Catch any '>' after the last quote.
  (sgml--syntax-propertize-ppss end))

(setq sgml-xml-mode t)
(defun sgml-syntax-propertize-inside (end)
  (let ((ppss (syntax-ppss)))
    (cond
     ((eq (nth 3 ppss) t)
      (let ((endre (save-excursion
                     (goto-char (nth 8 ppss))
                     (cond
                      ((looking-at-p "<!\\[CDATA\\[") "]]>")
                      ((looking-at-p "<\\?")  (if sgml-xml-mode "\\?>" ">"))
                      (t ">")))))
        (when (re-search-forward endre end 'move)
          (put-text-property (1- (point)) (point)
                             'syntax-table (string-to-syntax "|<"))))))))

;; sgml-font-lock-syntactic-face is needed by nxml-mode.el
(defun sgml-font-lock-syntactic-face (state)
  "`font-lock-syntactic-face-function' for `sgml-mode'."
  ;; Don't use string face outside of tags.
  (cond ((and (nth 9 state) (nth 3 state)) font-lock-string-face)
        ((nth 4 state) font-lock-comment-face)))

;; html-tag-alist is needed by css-mode.el
(defvar html-tag-alist
  (let* ((1-7 '(("1") ("2") ("3") ("4") ("5") ("6") ("7")))
	 (1-9 `(,@1-7 ("8") ("9")))
	 (align '(("align" ("left") ("center") ("right"))))
         (ialign '(("align" ("top") ("middle") ("bottom") ("left")
                    ("right"))))
	 (valign '(("top") ("middle") ("bottom") ("baseline")))
	 (rel '(("next") ("previous") ("parent") ("subdocument") ("made")))
	 (href '("href" ("ftp:") ("file:") ("finger:") ("gopher:") ("http:")
		 ("mailto:") ("news:") ("rlogin:") ("telnet:") ("tn3270:")
		 ("wais:") ("/cgi-bin/")))
	 (name '("name"))
	 (link `(,href
		 ("rel" ,@rel)
		 ("rev" ,@rel)
		 ("title")))
	 (list '((nil \n ("List item: " "<li>" str
                          (if sgml-xml-mode "</li>") \n))))
         (shape '(("shape" ("rect") ("circle") ("poly") ("default"))))
	 (cell `(t
		 ,@align
		 ("valign" ,@valign)
		 ("colspan" ,@1-9)
		 ("rowspan" ,@1-9)
		 ("nowrap" t)))
         (cellhalign '(("align" ("left") ("center") ("right")
                        ("justify") ("char"))
                       ("char") ("charoff")))
         (cellvalign '(("valign" ("top") ("middle") ("bottom")
                        ("baseline")))))
    ;; put ,-expressions first, else byte-compile chokes (as of V19.29)
    ;; and like this it's more efficient anyway
    `(("a" ,name ,@link)
      ("area" t ,@shape ("coords") ("href") ("nohref" "nohref") ("alt")
       ("tabindex") ("accesskey") ("onfocus") ("onblur"))
      ("base" t ,@href)
      ("col" t ,@cellhalign ,@cellvalign ("span") ("width"))
      ("colgroup" \n ,@cellhalign ,@cellvalign ("span") ("width"))
      ("dir" ,@list)
      ("figcaption")
      ("figure" \n)
      ("font" nil "size" ("-1") ("+1") ("-2") ("+2") ,@1-7)
      ("form" (\n _ \n "<input type=\"submit\" value=\"\""
	       (if sgml-xml-mode " />" ">"))
       ("action" ,@(cdr href)) ("method" ("get") ("post")))
      ("h1" ,@align)
      ("h2" ,@align)
      ("h3" ,@align)
      ("h4" ,@align)
      ("h5" ,@align)
      ("h6" ,@align)
      ("hr" t ("size" ,@1-9) ("width") ("noshade" t) ,@align)
      ("iframe" \n ,@ialign ("longdesc") ("name") ("src")
       ("frameborder" ("1") ("0")) ("marginwidth") ("marginheight")
       ("scrolling" ("yes") ("no") ("auto")) ("height") ("width"))
      ("img" t ("align" ,@valign ("texttop") ("absmiddle") ("absbottom"))
       ("src") ("alt") ("width" "1") ("height" "1")
       ("border" "1") ("vspace" "1") ("hspace" "1") ("ismap" t))
      ("input" t ,name ("accept") ("alt") ("autocomplete" ("on") ("off"))
       ("autofocus" t) ("checked" t) ("dirname") ("disabled" t) ("form")
       ("formaction")
       ("formenctype" ("application/x-www-form-urlencoded")
        ("multipart/form-data") ("text/plain"))
       ("formmethod" ("get") ("post"))
       ("formnovalidate" t)
       ("formtarget" ("_blank") ("_self") ("_parent") ("_top"))
       ("height") ("inputmode") ("list") ("max") ("maxlength") ("min")
       ("minlength") ("multiple" t) ("pattern") ("placeholder")
       ("readonly" t) ("required" t) ("size") ("src") ("step")
       ("type" ("hidden") ("text") ("search") ("tel") ("url") ("email")
        ("password") ("date") ("time") ("number") ("range") ("color")
        ("checkbox") ("radio") ("file") ("submit") ("image") ("reset")
        ("button"))
       ("value") ("width"))
      ("link" t ,@link)
      ("menu" ,@list)
      ("ol" ,@list ("type" ("A") ("a") ("I") ("i") ("1")))
      ("p" t ,@align)
      ("select" (nil \n
		     ("Text: "
		      "<option>" str (if sgml-xml-mode "</option>") \n))
       ,name ("size" ,@1-9) ("multiple" t))
      ("table" (nil \n
		    ((completing-read "Cell kind: " '(("td") ("th"))
				      nil t "t")
		     "<tr><" str ?> _
		     (if sgml-xml-mode (concat "<" str "></tr>")) \n))
       ("border" t ,@1-9) ("width" "10") ("cellpadding"))
      ("tbody" \n ,@cellhalign ,@cellvalign)
      ("td" ,@cell)
      ("textarea" ,name ("rows" ,@1-9) ("cols" ,@1-9))
      ("tfoot" \n ,@cellhalign ,@cellvalign)
      ("th" ,@cell)
      ("thead" \n ,@cellhalign ,@cellvalign)
      ("ul" ,@list ("type" ("disc") ("circle") ("square")))

      ;;,@sgml-tag-alist

      ("abbr")
      ("acronym")
      ("address")
      ("array" (nil \n
		    ("Item: " "<item>" str (if sgml-xml-mode "</item>") \n))
       "align")
      ("article" \n)
      ("aside" \n)
      ("au")
      ("audio" \n
       ("src") ("crossorigin" ("anonymous") ("use-credentials"))
       ("preload" ("none") ("metadata") ("auto"))
       ("autoplay" "autoplay") ("mediagroup") ("loop" "loop")
       ("muted" "muted") ("controls" "controls"))
      ("b")
      ("bdi")
      ("bdo" nil ("lang") ("dir" ("ltr") ("rtl")))
      ("big")
      ("blink")
      ("blockquote" \n ("cite"))
      ("body" \n ("background" ".gif") ("bgcolor" "#") ("text" "#")
       ("link" "#") ("alink" "#") ("vlink" "#"))
      ("box" (nil _ "<over>" _ (if sgml-xml-mode "</over>")))
      ("br" t ("clear" ("left") ("right")))
      ("button" nil ("name") ("value")
       ("type" ("submit") ("reset") ("button"))
       ("disabled" "disabled")
       ("tabindex") ("accesskey") ("onfocus") ("onblur"))
      ("canvas" \n ("width") ("height"))
      ("caption" ("valign" ("top") ("bottom")))
      ("center" \n)
      ("cite")
      ("code" \n)
      ("datalist" \n)
      ("dd" ,(not sgml-xml-mode))
      ("del" nil ("cite") ("datetime"))
      ("dfn")
      ("div" \n ("id") ("class"))
      ("dl" (nil \n
		 ( "Term: "
		   "<dt>" str (if sgml-xml-mode "</dt>")
                   "<dd>" _ (if sgml-xml-mode "</dd>") \n)))
      ("dt" (t _ (if sgml-xml-mode "</dt>")
             "<dd>" (if sgml-xml-mode "</dd>") \n))
      ("em")
      ("embed" t ("src") ("type") ("width") ("height"))
      ("fieldset" \n)
      ("fn" "id" "fn")  ;; Footnotes were deprecated in HTML 3.2
      ("footer" \n)
      ("frame" t ("longdesc") ("name") ("src")
       ("frameborder" ("1") ("0")) ("marginwidth") ("marginheight")
       ("noresize" "noresize") ("scrolling" ("yes") ("no") ("auto")))
      ("frameset" \n ("rows") ("cols") ("onload") ("onunload"))
      ("head" \n)
      ("header" \n)
      ("hgroup" \n)
      ("html" (\n
	       "<head>\n"
	       "<title>" (setq str (read-string "Title: ")) "</title>\n"
	       "</head>\n"
	       "<body>\n<h1>" str "</h1>\n" _
	       "\n<address>\n<a href=\"mailto:"
	       user-mail-address
	       "\">" (user-full-name) "</a>\n</address>\n"
	       "</body>"
		))
      ("i")
      ("ins" nil ("cite") ("datetime"))
      ("isindex" t ("action") ("prompt"))
      ("kbd")
      ("label" nil ("for") ("accesskey") ("onfocus") ("onblur"))
      ("lang")
      ("legend" nil ("accesskey"))
      ("li" ,(not sgml-xml-mode))
      ("main" \n)
      ("map" \n ("name"))
      ("mark")
      ("math" \n)
      ("meta" t ("http-equiv") ("name") ("content") ("scheme"))
      ("meter" nil ("value") ("min") ("max") ("low") ("high")
       ("optimum"))
      ("nav" \n)
      ("nobr")
      ("noframes" \n)
      ("noscript" \n)
      ("object" \n ("declare" "declare") ("classid") ("codebase")
       ("data") ("type") ("codetype") ("archive") ("standby")
       ("height") ("width") ("usemap") ("name") ("tabindex"))
      ("optgroup" \n ("name") ("size") ("multiple" "multiple")
       ("disabled" "disabled") ("tabindex") ("onfocus") ("onblur")
       ("onchange"))
      ("option" t ("value") ("label") ("selected" t))
      ("output" nil ("for") ("form") ("name"))
      ("over" t)
      ("param" t ("name") ("value")
       ("valuetype" ("data") ("ref") ("object")) ("type"))
      ("person") ;; Tag for person's name tag deprecated in HTML 3.2
      ("pre" \n)
      ("progress" nil ("value") ("max"))
      ("q" nil ("cite"))
      ("rev")
      ("rp" t)
      ("rt" t)
      ("ruby")
      ("s")
      ("samp")
      ("script" nil ("charset") ("type") ("src") ("defer" "defer"))
      ("section" \n)
      ("small")
      ("source" t ("src") ("type") ("media"))
      ("span" nil
	("class"
	 ("builtin")
	 ("comment")
	 ("constant")
	 ("function-name")
	 ("keyword")
	 ("string")
	 ("type")
	 ("variable-name")
	 ("warning")))
      ("strong")
      ("style" \n ("type") ("media") ("title"))
      ("sub")
      ("summary")
      ("sup")
      ("time" nil ("datetime"))
      ("title")
      ("tr" t)
      ("track" t
       ("kind" ("subtitles") ("captions") ("descriptions")
        ("chapters") ("metadata"))
       ("src") ("srclang") ("label") ("default"))
      ("tt")
      ("u")
      ("var")
      ("video" \n
       ("src") ("crossorigin" ("anonymous") ("use-credentials"))
       ("poster") ("preload" ("none") ("metadata") ("auto"))
       ("autoplay" "autoplay") ("mediagroup") ("loop" "loop")
       ("muted" "muted") ("controls" "controls") ("width") ("height"))
      ("wbr" t)))
  "Value of `sgml-tag-alist' for HTML mode.")

(provide 'sgml-mode-fix)
