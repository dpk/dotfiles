;; -*- lexical-binding: t; -*-
(require 'cl-lib)

(defvar sgml-show-indent-steps t
  "Non-nil if the SGML indent function should `show its working' in
the message bar when activated. (Only shows working when indent is
explicitly activated, not when activated by electric-indent-mode.)")

(defvar-local sgml-default-element-indent 1
  "The number of indentation steps an element should be indented by if
not otherwise specified by sgml-element-indent-alist.")

(defvar-local sgml-element-indent-alist '()
  "An alist mapping element names as strings to the number of indent
steps they use. If the number is nil, any existing indentation
within that element will never be touched.")

(defvar-local sgml-element-indent-stag '()
  "An list of element names (strings) whose start tags should be
considered lines to be indented according the rules of that
element.")

(defun sgml-element-stag-o-p (element)
  "True if element's start tag was omitted."
  (= (sgml-element-start element)
     (sgml-element-stag-end element)))

(defun sgml-element-ancestors (el)
  "List of all ancestors of the given element."
  (if (string-equal (sgml-element-gi el) "#DOC") nil
    (cons el (sgml-element-ancestors (sgml-element-parent el)))))

(defun sgml-element-to-indent (pos)
  (sgml-find-element-of pos))

(defun sgml-show-indent-steps-p ()
  (and sgml-show-indent-steps
       (not (memq 'newline (mapcar 'cadr (backtrace-frames))))))

(defun sgml-better-indent-line (&optional col element)
  "Attempt to indent lines in SGML (well, HTML) moderately more
intelligently than the regular sgml-indent-line function.

If an element's start tag is omitted in the document, it will not be
given an indentation level.

If an element's name is in sgml-element-indent-alist and associated to
a number, it will receive that many indentation steps on lines within
it; if associated to nil, its contents will be unaffected by
indentation.

All other elements receive sgml-default-element-indent indentation
steps by default.

If an element's name is in sgml-element-indent-stag, lines beginning
with the start tag for that element will also be indented according to
the rule for that element."
  (catch 'never-indent
    ;; line cursor point
    (let ((lcp (- (point) (line-beginning-position))))
      (let* ((sp (point)) (li (line-number-at-pos sp)))
        (beginning-of-line)
        (skip-chars-forward " \t")
        (when (not (= li (line-number-at-pos)))
          (goto-char sp)
          (beginning-of-line)
          (delete-horizontal-space)
          (if (not (= (point) sp))
              (throw 'never-indent "Not indenting an empty line."))))

      (let* ((el (sgml-element-to-indent (point)))
             (ancestors (sgml-element-ancestors el))
             (indent-levels-list (list 0)))
        (dolist (ancestor ancestors)
          (let* ((el-gi (sgml-element-gi ancestor))
                 (el-indent (alist-get el-gi
                                       sgml-element-indent-alist
                                       sgml-default-element-indent
                                       nil 'cl-equalp)))
            (cond ((and (null el-indent)
                        (< (line-number-at-pos (sgml-element-stag-end ancestor))
                           (line-number-at-pos)))
                   (if (sgml-show-indent-steps-p)
                       (message "Inside %s, not indenting." el-gi))
                   (goto-char (+ (line-beginning-position) lcp))
                   (throw 'never-indent
                          (format "Not indenting inside %s." el-gi)))
                  ((sgml-element-stag-o-p ancestor)
                   (push 0 indent-levels-list))
                  ((= (sgml-element-etag-start ancestor) (point))
                   (push 0 indent-levels-list))
                  ((and (= (line-number-at-pos (point))
                           (line-number-at-pos (sgml-element-start ancestor)))
                        (member-ignore-case el-gi sgml-element-indent-stag))
                   (push (max 0 (- el-indent 1)) indent-levels-list))
                  ((> (line-number-at-pos (point))
                      (line-number-at-pos (sgml-element-start ancestor)))
                   (push el-indent indent-levels-list))
                  (t (push 0 indent-levels-list)))))

        (if (sgml-show-indent-steps-p)
            (message "%s"
                     (mapconcat 'identity
                                (cl-mapcar
                                 #'(lambda (anc ind)
                                     (format "%s %d"
                                             (sgml-element-gi anc)
                                             ind))
                                 ancestors
                                 (cdr (reverse indent-levels-list)))
                                " < ")))
        (let* (;; line start point
               (lsp (progn (beginning-of-line) (point)))
               ;; printing characters start point
               (pcsp (progn (skip-chars-forward " \t") (point)))
               ;; number of whitespace chars to be deleted
               (nwscd (- pcsp lsp))
               ;; number of whitespace chars to be added
               (nwsca (* sgml-indent-step (apply '+ indent-levels-list))))
          (beginning-of-line)
          (delete-horizontal-space)
          (indent-to nwsca)
          ;; restore cursor position
          (goto-char (+ (line-beginning-position)
                        (- lcp nwscd)
                        nwsca)))))))

(defun sgml-better-indent-or-tab ()
  (interactive)
  (if sgml-indent-step (indent-for-tab-command) (insert-tab)))

(add-hook 'sgml-mode-hook
          #'(lambda () (setq indent-line-function 'sgml-better-indent-line)))
(define-key sgml-mode-map (kbd "<tab>") 'sgml-better-indent-or-tab)

(provide 'psgml-indent)
