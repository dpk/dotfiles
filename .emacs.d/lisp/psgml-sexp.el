;; -*- lexical-binding: t -*-

(defun sgml-forward-sexp (&optional arg)
  (let ((ntimes (if arg (abs arg) 1))
        (move-by-sexp (if (and arg (< arg 0))
                          'sgml-backward-element
                        'sgml-forward-element)))
    (condition-case err
        (dotimes (_ ntimes)
          (funcall move-by-sexp))
      (t (signal 'scan-error
                 (list (error-message-string err)
                       (sgml-element-start (sgml-find-element-of (point)))
                       (sgml-element-end (sgml-find-element-of (point)))))))))


(add-hook 'sgml-mode-hook
          #'(lambda ()
              (setq-local forward-sexp-function
                          'sgml-forward-sexp)))

(provide 'psgml-sexp)
