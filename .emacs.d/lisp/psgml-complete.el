;; -*- lexical-binding: t; -*-
;; Derived from sgml-complete in psgml-edit.el.
;; This file is GPLv2+, because PSGML is GPL v2+.

(defun sgml-completion-at-point-function ()
  (let ((tab				; The completion table
	 nil)
        (ignore-case                    ; If ignore case in matching completion
         sgml-namecase-general)
        (insert-case
         'sgml-general-insert-case)
	(pattern nil)
	(c nil)
	(here (point))
        (there (point)))
    (save-excursion
      (skip-chars-backward "^ \n\t</!&%#")
      (setq there (point))
      (list
       there here
       (completion-table-dynamic
        (lambda (_)
          (save-excursion
            (goto-char there)
            (setq pattern (buffer-substring (point) here))
            (setq c (char-after (1- (point))))
            (cond
             ;; entity
             ((eq c ?&)
              (sgml-need-dtd)
              (setq insert-case 'sgml-entity-insert-case)
              (setq tab
	            (sgml-entity-completion-table
	             (sgml-dtd-entities (sgml-pstate-dtd sgml-buffer-parse-state)))))
             ;; start-tag
             ((eq c ?<)
              (save-excursion
	        (backward-char 1)
	        (sgml-parse-to-here)
	        (setq tab (sgml-eltype-completion-table
		           (sgml-current-list-of-valid-eltypes)))))
             ;; end-tag
             ((eq c ?/)
              (save-excursion
	        (backward-char 2)
	        (sgml-parse-to-here)
	        (setq tab (sgml-eltype-completion-table
		           (sgml-current-list-of-endable-eltypes)))))
             ;; markup declaration
             ((eq c ?!)
              (setq tab sgml-markup-declaration-table
                    ignore-case t))
             ;; Reserved words with '#' prefix
             ((eq c ?#)
              (setq tab '(("PCDATA") ("NOTATION") ("IMPLIED") ("REQUIRED")
                          ("FIXED") ("EMPTY"))
                    ignore-case t)))
            (if (and sgml-namecase-general
                     (eq insert-case 'sgml-general-insert-case)
                     (eq sgml-general-insert-case 'lower))
                (setq tab (mapcar (lambda (x)
                                    (cons (downcase (car x)) (cdr x)))
                                  tab)))
            (if ignore-case
                (completion-table-case-fold tab)
              tab))))))))

(add-hook 'sgml-mode-hook
          #'(lambda () (add-hook 'completion-at-point-functions #'sgml-completion-at-point-function nil 'local)))
;; Defang sgml-complete
(define-key sgml-mode-map (kbd "C-M-i") nil)

(provide 'psgml-complete)
