;;; quail-indo-european.el --- Quail package for PIE and descendant proto-languages  -*-coding: utf-8; lexical-binding: t; -*-
;;
;;; Commentary:
;; 

;;; Code:

(require 'quail)


(quail-define-package
 "indo-european" "Indo-European" "IE" t
 "Input method for typing words in reconstructed Indo-European
languages, with some helpers for typing recorded ancient
languages in Latin transliteration.

* General conventions

** Accents below

Accents below are typed by punctuation *before* the character following.

- ^ before a vowel adds an inverted breve below it. (PIE semivowels.)

- . before a consonant adds a ring below it. (PIE syllabic consonants.)

** Accents above

Accents above are typed by punctuation *above* the character following.

- ' after a consonant adds an acute accent or an inverted breve
  above it. (PIE palatovelars.)

- _ after a vowel adds a macron over it.

- ~ after a vowel adds a breve over it.

- ' after a vowel adds an acute over it.

Where it makes sense, these can be combined in the order they are
listed here, thus a_~ is an a with both macron and breve, e_' is
an e with macron and acute accent, etc.

** Further conventions

- _ after a letter (invariably a consonant) adds a subscript or
  subscript to it. (It is thus assumed you will rarely want to
  write something like a₂ or eʰ, because this use of _ conflicts
  with the use for macrons on vowels).

* Post-PIE languages

The conventions above are extended to characters not actually
known to represent characters in PIE, but which may be used
either in older reconstructions of PIE or in reconstructions of
intermediate languages.

** Proto-Indo-Iranian

The convention of ~ after a vowel is extended to add a caron when
used after consonants.

The convention of ' after a vowel is extended to add an acute to
consonants.

** Proto-Italic

xx   chi

** Proto-Germanic

The convention of ~ after a vowel is extended to add a bar to b,
d, and g. (_ is not used for this purpose to avoid conflicting
with its use for super- and subscripts.)

A convention of , before a vowel is used to add an ogonek.

\ae  ash
\oe  oe ligature
xx   chi
&    thorn

* Attested languages

Some characters conventionally used only in the transliteration
of actual attested languages are available for convenience, using
letters prefixed by +. These are:

** Indo-Iranian languages

Sanskrit (IAST): +. followed by r, l, n, s, t, d, h, or m gets a
version of that letter with a dot below; add an extra _ after r
or l to also get a macron. +n. gets you n with a dot above; +n~
gets you n with a tilde above.

Avestan: In a sequence beginning with +, the usual rules apply to
place macrons, acutes, dots, carons, etc.

+G    gamma
+D    delta
+T    theta
+B    beta
+N    eng
+_v   superscript v
+~t   t with tilde below
+,m   m with ogonek

** Germanic languages

+hw, +Hw   Gothic hwair (lower- and upper-case)
+dh, +Dh   Germanic (various) eth (lower- and upper-case)
+th, +Th   Germanic (various) thorn (lower- and upper-case)

* Miscellaneous special characters used in Indo-European studies

/0    ∅ (disappearance of a sound)"
 nil nil nil nil nil nil nil nil nil nil t)


(quail-define-rules
 ("_w" ["ᵘ̯" "ʷ"])
 ("_h" ?ʰ)
 ("_wh" ["ᵘ̯ʰ" "ʷʰ"]) ; conflict if anyone ever wanted e.g. kʷh₁, but this
                     ; is not actually possible in PIE phonology afaik
 ("_1" ?₁)
 ("_2" ?₂)
 ("_3" ?₃)
 ("_4" ?₄)
 ("_5" ?₅)
 ("_6" ?₆)
 ("_7" ?₇)
 ("_8" ?₈)
 ("_9" ?₉) ; hopefully nobody reconstructs *this* many laryngeals ...
 ("_0" ?₀) ; ... but if they did, this might be handy
 ("_a" ?ₐ) ; hₐ is used to mean either h₂ or h₄ by those who are into
           ; that sort of thing; sometimes also just h₂
 ("_e" ?ₑ)
 ("_o" ?ₒ)
 ("k'" ["k̑" "ḱ"])
 ("g'" ["g̑" "ǵ"])

 ;; vowels
 ("@" ?ə)
 ("@_" ["ə̄"])
 ("@_1" ["ə₁"]) ; special cases, as schwa is sometimes used to notate
 ("@_2" ["ə₂"]) ; laryngeals ...
 ("@_3" ["ə₃"]) ; ... but hopefully no more than three
 ("a_" ?ā)
 ("a~" ?ă)
 ("a'" ?á)
 ("a_~" ["ā\u0306"])
 ("a_'" ["ā\u0301"])
 ("a_~'" ["ā\u0306\u0301"])
 ("e_" ?ē)
 ("e~" ?ĕ)
 ("e'" ?é)
 ("e_'" ?ḗ)
 ("i_" ?ī)
 ("i~" ?ĭ)
 ("i_'" ["ī\u0301"])
 ("o_" ?ō)
 ("o~" ?ŏ)
 ("o_~" ["ō\u0308"])
 ("o'" ?ó)

 ;; semivowels
 ("^i" ["i̯"])
 ("^u" ["u̯"])

 ;; syllabics
 (".l" ["l̥"])
 (".l'" ["l̥\u0301"])
 (".r" ["r̥"])
 (".r'" ["r̥\u0301"])
 (".n" ["n̥"])
 (".n'" ["n̥\u0301"])
 (".m" ["m̥"])
 (".m'" ["m̥\u0301"])

 ;; for Proto-Indo-Iranian
 ("c'" ?ć)
 ("j'" ["j\u0301"])
 ("s~" ?š)
 ("s~'" ["š\u0301"])
 ("s'" ?ś)
 ("z~" ?ž)
 ;; ... for Sanskrit (IAST)
 ("+.r" ?ṛ)
 ("+.r_" ?ṝ)
 ("+.l" ?ḷ)
 ("+.l_" ?ḹ)
 ("+.n" ?ṇ)
 ("+n." ?ṅ)
 ("+n~" ?ñ)
 ("+.s" ?ṣ)
 ("+.t" ?ṭ)
 ("+.d" ?ḍ)
 ("+.h" ?ḥ)
 ("+.m" ?ṃ)
 ;; ... for Avestan (after Hoffmann and Bartholomae)
 ("+a." ?å)
 ("+a_." ["ā\u030A"])
 ("+,a." ["ą̇"])
 ("+h." ?ḣ)
 ("+g." ?ġ)
 ("+G" ?γ)
 ("+B" ?β)
 ("+N" ?ŋ)
 ("+N'" ["ŋ́"])
 ("+D" ["δ"])
 ("+n'" ?ń)
 ("+,m" ["m̨"])
 ("+T" ?θ)
 ("+~t" ["t̰"])
 ("+.s~" ["ṣ̌"])
 ("+x'" ["x́"])
 ("+_v" ?ᵛ)
 ("+y." ?ẏ)

 ;; for Proto-Italic
 ("xx" ?ꭓ)

 ;; for Proto-Germanic
 ("b~" ?ƀ)
 ("d~" ?đ)
 ("g~" ?ǥ)
 (",a" ?ą)
 (",e" ?ę)
 (",i" ?į)
 (",o" ?ǫ)
 (",u" ?ų)
 ("\\ae" ?æ)
 ("\\ae_" ?ǣ)
 ("\\ae_'" ["ǣ\u0301"])
 ("\\oe" ?œ)
 ("\\oe_" ["œ̄"])
 ("&" ?þ)
 ;; ... for Gothic
 ("+hw" ?ƕ)
 ("+Hw" ?Ƕ)
 ;; ... for Old English, Old Saxon, Icelandic, etc.
 ("+dh" ?ð)
 ("+Dh" ?Ð)
 ("+th" ?þ) ; also &, but for consistency
 ("+Th" ?Þ)

 ("/0" "∅")
)

(provide 'quail-indo-european)

;;; quail-indo-european.el ends here
