
(deftheme vimspectr150-dark
  "The vimspectr150-dark colour scheme (by nightsense, ported to Emacs by dpk)")

(custom-theme-set-faces
  'vimspectr150-dark
  `(default ((t :foreground ,vimspectr150-dark-g5 :background ,vimspectr150-dark-g0 )))
  `(cursor  ((t :foreground ,vimspectr150-dark-g0 :background ,vimspectr150-dark-g5 )))
  `(error   ((t :foreground ,vimspectr150-dark-g8 :background ,vimspectr150-dark-g0 )))
  `(isearch ((t :foreground ,vimspectr150-dark-gA :background ,vimspectr150-dark-g0 :inverse-video t )))
  `(query-replace ((t :foreground ,vimspectr150-dark-gA :background ,vimspectr150-dark-g0 :inverse-video t )))
  `(highlight ((t :foreground ,vimspectr150-dark-gA :background ,vimspectr150-dark-g0 :inverse-video t )))
  `(lazy-highlight ((t :foreground ,vimspectr150-dark-g9 :background ,vimspectr150-dark-g0 :inverse-video t )))

  ;; no equivalent of button or link in vim
  `(button ((t :foreground ,vimspectr150-dark-gD :underline t)))
  `(link ((t :foreground ,vimspectr150-dark-g4 :underline t)))

  `(mode-line ((t :foreground ,vimspectr150-dark-g0 :background ,vimspectr150-dark-g5 )))
  ;; no equivalent of minibuffer in vim so let's make something up
  `(minibuffer-prompt ((t :foreground ,vimspectr150-dark-gC)))

  `(fringe ((t :foreground ,vimspectr150-dark-g5 :background ,vimspectr150-dark-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  `(linum  ((t :foreground ,vimspectr150-dark-g5 :background ,vimspectr150-dark-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  `(line-number ((t :foreground ,vimspectr150-dark-g5 :background ,vimspectr150-dark-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  ;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Faces-for-Font-Lock.html#Faces-for-Font-Lock
  `(font-lock-warning-face       ((t :foreground ,vimspectr150-dark-g8 :background ,vimspectr150-dark-g0 )))
  `(font-lock-function-name-face ((t :foreground ,vimspectr150-dark-gF )))
  `(font-lock-variable-name-face ((t :foreground ,vimspectr150-dark-gF )))
  `(font-lock-keyword-face       ((t :foreground ,vimspectr150-dark-gB )))
  `(font-lock-builtin-face       ((t :foreground ,vimspectr150-dark-gB )))
  `(font-lock-comment-face       ((t :foreground ,vimspectr150-dark-g2 )))
  `(font-lock-comment-delimiter-face ((t :foreground ,vimspectr150-dark-g2 )))
  `(font-lock-type-face          ((t :foreground ,vimspectr150-dark-gC )))
  `(font-lock-preprocessor-face  ((t :foreground ,vimspectr150-dark-g9 )))
  `(font-lock-constant-face      ((t :foreground ,vimspectr150-dark-gD )))
  `(font-lock-string-face        ((t :foreground ,vimspectr150-dark-gD )))

  `(diff-added   ((t :foreground ,vimspectr150-dark-gB :background ,vimspectr150-dark-g0 )))
  `(diff-removed ((t :foreground ,vimspectr150-dark-g8 :background ,vimspectr150-dark-g0 )))
  `(diff-changed ((t :foreground ,vimspectr150-dark-g2 :background ,vimspectr150-dark-g0 )))
  `(diff-refine-changed ((t :foreground ,vimspectr150-dark-g9 :background ,vimspectr150-dark-g0 )))
  `(diff-refine-added   ((t :foreground ,vimspectr150-dark-g9 :background ,vimspectr150-dark-g0 )))
  `(diff-refine-removed ((t :foreground ,vimspectr150-dark-g9 :background ,vimspectr150-dark-g0 )))

  ;; contrast from pink for name to red for error isn't big enough with ErrorMsg
  `(rng-error     ((t :foreground ,vimspectr150-dark-g8 :background ,vimspectr150-dark-g0 :inverse-video t )))

  `(flyspell-incorrect ((t :underline (:color ,vimspectr150-dark-g8 :style wave) )))
  ;; SpellCap is actually 'miscapitalized word' in vim but emacs doesn't have that
  `(flyspell-duplicate ((t :underline (:color ,vimspectr150-dark-g9 :style wave) )))

  `(show-paren-match    ((t :background ,vimspectr150-dark-g1 )))
  `(show-paren-mismatch ((t :foreground ,vimspectr150-dark-g8 :background ,vimspectr150-dark-g0 :inverse-video t )))
)

(provide-theme 'vimspectr150-dark)

