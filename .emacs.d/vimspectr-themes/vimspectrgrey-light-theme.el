
(deftheme vimspectrgrey-light
  "The vimspectrgrey-light colour scheme (by nightsense, ported to Emacs by dpk)")

(custom-theme-set-faces
  'vimspectrgrey-light
  `(default ((t :foreground ,vimspectrgrey-light-g5 :background ,vimspectrgrey-light-g0 )))
  `(cursor  ((t :foreground ,vimspectrgrey-light-g0 :background ,vimspectrgrey-light-g5 )))
  `(error   ((t :foreground ,vimspectrgrey-light-g8 :background ,vimspectrgrey-light-g0 )))
  `(isearch ((t :foreground ,vimspectrgrey-light-gA :background ,vimspectrgrey-light-g6 :inverse-video t )))
  `(query-replace ((t :foreground ,vimspectrgrey-light-gA :background ,vimspectrgrey-light-g6 :inverse-video t )))
  `(highlight ((t :foreground ,vimspectrgrey-light-gA :background ,vimspectrgrey-light-g6 :inverse-video t )))
  `(lazy-highlight ((t :foreground ,vimspectrgrey-light-g9 :background ,vimspectrgrey-light-g0 :inverse-video t )))

  ;; no equivalent of button or link in vim
  `(button ((t :foreground ,vimspectrgrey-light-gD :underline t)))
  `(link ((t :foreground ,vimspectrgrey-light-g4 :underline t)))

  `(mode-line ((t :foreground ,vimspectrgrey-light-g0 :background ,vimspectrgrey-light-g5 )))
  ;; no equivalent of minibuffer in vim so let's make something up
  `(minibuffer-prompt ((t :foreground ,vimspectrgrey-light-gC)))

  `(fringe ((t :foreground ,vimspectrgrey-light-g5 :background ,vimspectrgrey-light-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  `(linum  ((t :foreground ,vimspectrgrey-light-g5 :background ,vimspectrgrey-light-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  `(line-number ((t :foreground ,vimspectrgrey-light-g5 :background ,vimspectrgrey-light-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  ;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Faces-for-Font-Lock.html#Faces-for-Font-Lock
  `(font-lock-warning-face       ((t :foreground ,vimspectrgrey-light-g8 :background ,vimspectrgrey-light-g0 )))
  `(font-lock-function-name-face ((t :foreground ,vimspectrgrey-light-gF )))
  `(font-lock-variable-name-face ((t :foreground ,vimspectrgrey-light-gF )))
  `(font-lock-keyword-face       ((t :foreground ,vimspectrgrey-light-gB )))
  `(font-lock-builtin-face       ((t :foreground ,vimspectrgrey-light-gB )))
  `(font-lock-comment-face       ((t :foreground ,vimspectrgrey-light-g2 )))
  `(font-lock-comment-delimiter-face ((t :foreground ,vimspectrgrey-light-g2 )))
  `(font-lock-type-face          ((t :foreground ,vimspectrgrey-light-gC )))
  `(font-lock-preprocessor-face  ((t :foreground ,vimspectrgrey-light-g9 )))
  `(font-lock-constant-face      ((t :foreground ,vimspectrgrey-light-gD )))
  `(font-lock-string-face        ((t :foreground ,vimspectrgrey-light-gD )))

  `(diff-added   ((t :foreground ,vimspectrgrey-light-gB :background ,vimspectrgrey-light-g0 )))
  `(diff-removed ((t :foreground ,vimspectrgrey-light-g8 :background ,vimspectrgrey-light-g0 )))
  `(diff-changed ((t :foreground ,vimspectrgrey-light-g2 :background ,vimspectrgrey-light-g0 )))
  `(diff-refine-changed ((t :foreground ,vimspectrgrey-light-g9 :background ,vimspectrgrey-light-g0 )))
  `(diff-refine-added   ((t :foreground ,vimspectrgrey-light-g9 :background ,vimspectrgrey-light-g0 )))
  `(diff-refine-removed ((t :foreground ,vimspectrgrey-light-g9 :background ,vimspectrgrey-light-g0 )))

  ;; contrast from pink for name to red for error isn't big enough with ErrorMsg
  `(rng-error     ((t :foreground ,vimspectrgrey-light-g8 :background ,vimspectrgrey-light-g0 :inverse-video t )))

  `(flyspell-incorrect ((t :underline (:color ,vimspectrgrey-light-g8 :style wave) )))
  ;; SpellCap is actually 'miscapitalized word' in vim but emacs doesn't have that
  `(flyspell-duplicate ((t :underline (:color ,vimspectrgrey-light-g9 :style wave) )))

  `(show-paren-match    ((t :background ,vimspectrgrey-light-g1 )))
  `(show-paren-mismatch ((t :foreground ,vimspectrgrey-light-g8 :background ,vimspectrgrey-light-g0 :inverse-video t )))
)

(provide-theme 'vimspectrgrey-light)

