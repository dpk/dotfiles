
(deftheme vimspectr60-light
  "The vimspectr60-light colour scheme (by nightsense, ported to Emacs by dpk)")

(custom-theme-set-faces
  'vimspectr60-light
  `(default ((t :foreground ,vimspectr60-light-g5 :background ,vimspectr60-light-g0 )))
  `(cursor  ((t :foreground ,vimspectr60-light-g0 :background ,vimspectr60-light-g5 )))
  `(error   ((t :foreground ,vimspectr60-light-g8 :background ,vimspectr60-light-g0 )))
  `(isearch ((t :foreground ,vimspectr60-light-gA :background ,vimspectr60-light-g6 :inverse-video t )))
  `(query-replace ((t :foreground ,vimspectr60-light-gA :background ,vimspectr60-light-g6 :inverse-video t )))
  `(highlight ((t :foreground ,vimspectr60-light-gA :background ,vimspectr60-light-g6 :inverse-video t )))
  `(lazy-highlight ((t :foreground ,vimspectr60-light-g9 :background ,vimspectr60-light-g0 :inverse-video t )))

  ;; no equivalent of button or link in vim
  `(button ((t :foreground ,vimspectr60-light-gD :underline t)))
  `(link ((t :foreground ,vimspectr60-light-g4 :underline t)))

  `(mode-line ((t :foreground ,vimspectr60-light-g0 :background ,vimspectr60-light-g5 )))
  ;; no equivalent of minibuffer in vim so let's make something up
  `(minibuffer-prompt ((t :foreground ,vimspectr60-light-gC)))

  `(fringe ((t :foreground ,vimspectr60-light-g5 :background ,vimspectr60-light-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  `(linum  ((t :foreground ,vimspectr60-light-g5 :background ,vimspectr60-light-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  `(line-number ((t :foreground ,vimspectr60-light-g5 :background ,vimspectr60-light-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  ;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Faces-for-Font-Lock.html#Faces-for-Font-Lock
  `(font-lock-warning-face       ((t :foreground ,vimspectr60-light-g8 :background ,vimspectr60-light-g0 )))
  `(font-lock-function-name-face ((t :foreground ,vimspectr60-light-gF )))
  `(font-lock-variable-name-face ((t :foreground ,vimspectr60-light-gF )))
  `(font-lock-keyword-face       ((t :foreground ,vimspectr60-light-gB )))
  `(font-lock-builtin-face       ((t :foreground ,vimspectr60-light-gB )))
  `(font-lock-comment-face       ((t :foreground ,vimspectr60-light-g2 )))
  `(font-lock-comment-delimiter-face ((t :foreground ,vimspectr60-light-g2 )))
  `(font-lock-type-face          ((t :foreground ,vimspectr60-light-gC )))
  `(font-lock-preprocessor-face  ((t :foreground ,vimspectr60-light-g9 )))
  `(font-lock-constant-face      ((t :foreground ,vimspectr60-light-gD )))
  `(font-lock-string-face        ((t :foreground ,vimspectr60-light-gD )))

  `(diff-added   ((t :foreground ,vimspectr60-light-gB :background ,vimspectr60-light-g0 )))
  `(diff-removed ((t :foreground ,vimspectr60-light-g8 :background ,vimspectr60-light-g0 )))
  `(diff-changed ((t :foreground ,vimspectr60-light-g2 :background ,vimspectr60-light-g0 )))
  `(diff-refine-changed ((t :foreground ,vimspectr60-light-g9 :background ,vimspectr60-light-g0 )))
  `(diff-refine-added   ((t :foreground ,vimspectr60-light-g9 :background ,vimspectr60-light-g0 )))
  `(diff-refine-removed ((t :foreground ,vimspectr60-light-g9 :background ,vimspectr60-light-g0 )))

  ;; contrast from pink for name to red for error isn't big enough with ErrorMsg
  `(rng-error     ((t :foreground ,vimspectr60-light-g8 :background ,vimspectr60-light-g0 :inverse-video t )))

  `(flyspell-incorrect ((t :underline (:color ,vimspectr60-light-g8 :style wave) )))
  ;; SpellCap is actually 'miscapitalized word' in vim but emacs doesn't have that
  `(flyspell-duplicate ((t :underline (:color ,vimspectr60-light-g9 :style wave) )))

  `(show-paren-match    ((t :background ,vimspectr60-light-g1 )))
  `(show-paren-mismatch ((t :foreground ,vimspectr60-light-g8 :background ,vimspectr60-light-g0 :inverse-video t )))
)

(provide-theme 'vimspectr60-light)

