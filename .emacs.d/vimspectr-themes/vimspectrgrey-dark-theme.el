
(deftheme vimspectrgrey-dark
  "The vimspectrgrey-dark colour scheme (by nightsense, ported to Emacs by dpk)")

(custom-theme-set-faces
  'vimspectrgrey-dark
  `(default ((t :foreground ,vimspectrgrey-dark-g5 :background ,vimspectrgrey-dark-g0 )))
  `(cursor  ((t :foreground ,vimspectrgrey-dark-g0 :background ,vimspectrgrey-dark-g5 )))
  `(error   ((t :foreground ,vimspectrgrey-dark-g8 :background ,vimspectrgrey-dark-g0 )))
  `(isearch ((t :foreground ,vimspectrgrey-dark-gA :background ,vimspectrgrey-dark-g0 :inverse-video t )))
  `(query-replace ((t :foreground ,vimspectrgrey-dark-gA :background ,vimspectrgrey-dark-g0 :inverse-video t )))
  `(highlight ((t :foreground ,vimspectrgrey-dark-gA :background ,vimspectrgrey-dark-g0 :inverse-video t )))
  `(lazy-highlight ((t :foreground ,vimspectrgrey-dark-g9 :background ,vimspectrgrey-dark-g0 :inverse-video t )))

  ;; no equivalent of button or link in vim
  `(button ((t :foreground ,vimspectrgrey-dark-gD :underline t)))
  `(link ((t :foreground ,vimspectrgrey-dark-g4 :underline t)))

  `(mode-line ((t :foreground ,vimspectrgrey-dark-g0 :background ,vimspectrgrey-dark-g5 )))
  ;; no equivalent of minibuffer in vim so let's make something up
  `(minibuffer-prompt ((t :foreground ,vimspectrgrey-dark-gC)))

  `(fringe ((t :foreground ,vimspectrgrey-dark-g5 :background ,vimspectrgrey-dark-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  `(linum  ((t :foreground ,vimspectrgrey-dark-g5 :background ,vimspectrgrey-dark-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  `(line-number ((t :foreground ,vimspectrgrey-dark-g5 :background ,vimspectrgrey-dark-g1 :weight normal :underline nil :slant normal :inverse-video nil :box nil :strike-through nil :overline nil)))
  ;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Faces-for-Font-Lock.html#Faces-for-Font-Lock
  `(font-lock-warning-face       ((t :foreground ,vimspectrgrey-dark-g8 :background ,vimspectrgrey-dark-g0 )))
  `(font-lock-function-name-face ((t :foreground ,vimspectrgrey-dark-gF )))
  `(font-lock-variable-name-face ((t :foreground ,vimspectrgrey-dark-gF )))
  `(font-lock-keyword-face       ((t :foreground ,vimspectrgrey-dark-gB )))
  `(font-lock-builtin-face       ((t :foreground ,vimspectrgrey-dark-gB )))
  `(font-lock-comment-face       ((t :foreground ,vimspectrgrey-dark-g2 )))
  `(font-lock-comment-delimiter-face ((t :foreground ,vimspectrgrey-dark-g2 )))
  `(font-lock-type-face          ((t :foreground ,vimspectrgrey-dark-gC )))
  `(font-lock-preprocessor-face  ((t :foreground ,vimspectrgrey-dark-g9 )))
  `(font-lock-constant-face      ((t :foreground ,vimspectrgrey-dark-gD )))
  `(font-lock-string-face        ((t :foreground ,vimspectrgrey-dark-gD )))

  `(diff-added   ((t :foreground ,vimspectrgrey-dark-gB :background ,vimspectrgrey-dark-g0 )))
  `(diff-removed ((t :foreground ,vimspectrgrey-dark-g8 :background ,vimspectrgrey-dark-g0 )))
  `(diff-changed ((t :foreground ,vimspectrgrey-dark-g2 :background ,vimspectrgrey-dark-g0 )))
  `(diff-refine-changed ((t :foreground ,vimspectrgrey-dark-g9 :background ,vimspectrgrey-dark-g0 )))
  `(diff-refine-added   ((t :foreground ,vimspectrgrey-dark-g9 :background ,vimspectrgrey-dark-g0 )))
  `(diff-refine-removed ((t :foreground ,vimspectrgrey-dark-g9 :background ,vimspectrgrey-dark-g0 )))

  ;; contrast from pink for name to red for error isn't big enough with ErrorMsg
  `(rng-error     ((t :foreground ,vimspectrgrey-dark-g8 :background ,vimspectrgrey-dark-g0 :inverse-video t )))

  `(flyspell-incorrect ((t :underline (:color ,vimspectrgrey-dark-g8 :style wave) )))
  ;; SpellCap is actually 'miscapitalized word' in vim but emacs doesn't have that
  `(flyspell-duplicate ((t :underline (:color ,vimspectrgrey-dark-g9 :style wave) )))

  `(show-paren-match    ((t :background ,vimspectrgrey-dark-g1 )))
  `(show-paren-mismatch ((t :foreground ,vimspectrgrey-dark-g8 :background ,vimspectrgrey-dark-g0 :inverse-video t )))
)

(provide-theme 'vimspectrgrey-dark)

