{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nix-darwin = {
      url = "github:LnL7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ self, nix-darwin, home-manager, nixpkgs }:
    let
      configuration = {pkgs, ...}: {
        services.nix-daemon.enable = true;
        nix.settings.experimental-features = "nix-command flakes";

        system.configurationRevision = self.rev or self.dirtyRev or null;
        system.stateVersion = 4;

        nixpkgs.hostPlatform = "aarch64-darwin";

        users.users.dpk = {
          name = "dpk";
          home = "/Users/dpk";
        };

        programs.fish.enable = true;

        environment.systemPackages = [
          pkgs.fish
        ];
      };
    in
      {
        darwinConfigurations.Copernicus = nix-darwin.lib.darwinSystem {
          modules = [
            configuration
            home-manager.darwinModules.home-manager {
              home-manager.users.dpk = { pkgs, ... }: {
                programs.man.enable = false;
                home.stateVersion = "24.05";
                home.packages = [
                  pkgs.borgbackup
                  pkgs.chez
                  pkgs.chibi
                  pkgs.ffmpeg
                  pkgs.guile
                  pkgs.graphviz
                  (pkgs.callPackage ./pkg/jdupes/jdupes.nix {})
                  #pkgs.mosh
                  pkgs.mu
                  pkgs.offlineimap
                  pkgs.ollama
                  pkgs.parallel
                  pkgs.ripgrep
                  (pkgs.callPackage ./pkg/sgrep/sgrep.nix {})
                  pkgs.wget
                  pkgs.yt-dlp
                  pkgs.zopfli
                  pkgs.zstd
                ];
              };
            }
          ];
        };
      };
}
