if test (uname) = "Darwin"
    setenv EDITOR "emacsclient"
end

if test -d /usr/local/opt/go/libexec/bin
    set -gx PATH $PATH /usr/local/opt/go/libexec/bin
    set -gx PATH $PATH $HOME/.go/bin
    setenv GOPATH $HOME/.go
end

set -gx PATH $PATH $HOME/bin
set -gx PATH $PATH $HOME/.cargo/bin

set -gx PATH $PATH /usr/local/opt/gambit-scheme/current/bin
set -gx PATH $PATH /usr/local/opt/gerbil-scheme/libexec/bin

alias ec emacsclient
alias ecn "emacsclient -n"
alias giff "git diff --no-index"
alias 'o.' 'open .'

function mkcd
    mkdir -p $argv
    cd $argv
end

set -gx HOMEBREW_NO_EMOJI 1
setenv HOMEBREW_NO_INSTALL_CLEANUP 1

set -gx PYENV_ROOT $HOME/.pyenv
if test -d $PYENV_ROOT/bin
   set -gx PATH $PYENV_ROOT/bin $PATH
   status --is-interactive; and . (pyenv init -|psub)
end

# pyenv init
# status is-login; and pyenv init --path | source
# status is-interactive; and pyenv init - | source

# # rbenv init
# status is-interactive; and rbenv init - fish | source

set -gx CHEZSCHEMELIBDIRS "$HOME/.chez-lib:"

set -gx LESS "--mouse -R"

# See https://github.com/fish-shell/fish-shell/issues/7511
function __fish_describe_command; end
